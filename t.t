! T3X9 -> ELF-FreeBSD-386 compiler
! Nils M Holm, 2017, CC0 license
! https://creativecommons.org/publicdomain/zero/1.0/
! Patchlevel 4

const	BPW = 8;
const	BYTES_REL = 4;

const	PROG_SIZE = 65536;

const	TEXT_SIZE = 65536;  ! must be a multiple of PAGE_SIZE !
const	DATA_SIZE = 65536;

const	NRELOC = 10000;

const	STACK_SIZE = 100;

const	SYMTBL_SIZE = 1000;
const	NLIST_SIZE = 10000;

var	Stack[STACK_SIZE], Sp;

var	Line;
var	GlobSize;

const	O_RDONLY = 0;
const	O_WRONLY = 1;
const	O_CREAT  = 64;
const	O_TRUNC  = 512;


var     OutputFile;
var     InputFile;

const	ENDFILE = %1;

var ntoa_buf::100;

ntoa(x) do var i, k;
	if (x = 0) return "0";
	i := 0;
	k := x<0-> -x: x;
	while (k > 0) do
		i := i+1;
		k := k/10;
	end
	i := i+1;
	if (x < 0) i := i+1;
	ntoa_buf::i := 0;
	k := x<0-> -x: x;
	while (k > 0) do
		i := i-1;
		ntoa_buf::i := '0' + k mod 10;
		k := k/10;
	end
	if (x < 0) do
		i := i-1;
		ntoa_buf::i := '-';
	end
	return @ntoa_buf::i;
end

str.length(s) return t.memscan(s, 0, 32767);

str.copy(sd, ss) t.memcopy(ss, sd, str.length(ss)+1);

str.append(sd, ss) t.memcopy(ss, @sd::str.length(sd), str.length(ss)+1);

str.equal(s1, s2) return t.memcomp(s1, s2, str.length(s1)+1) = 0;

writes(s) t.write(OutputFile, s, str.length(s));

log(s) t.write(2, s, str.length(s));

aw(m, s) do
	log("t3x9: ");
	log(ntoa(Line));
	log(": ");
	log(m);
	if (s \= 0) do
		log(": ");
		log(s);
	end
	log("\n");
	halt 1;
end

oops(m, s) do
	log("t3x9: internal error\n");
	aw(m, s);
end

push(x) do
	if (Sp >= STACK_SIZE) oops("stack overflow", 0);
	Stack[Sp] := x;
	Sp := Sp+1;
end

tos() return Stack[Sp-1];

pop() do
	if (Sp < 1) oops("stack underflow", 0);
	Sp := Sp-1;
	return Stack[Sp];
end

swap() do var t;
	if (Sp < 2) oops("stack underflow", 0);
	t := Stack[Sp-1];
	Stack[Sp-1] := Stack[Sp-2];
	Stack[Sp-2] := t;
end

numeric(c) return '0' <= c /\ c <= '9';

alphabetic(c) return 'a' <= c /\ c <= 'z' \/
		     'A' <= c /\ c <= 'Z';

!
! Symbol table
!

struct	SYM = SNAME, SFLAGS, SVALUE;

const	GLOBF = 1;
const	CNST  = 2;
const	VECT  = 4;
const	FORW  = 8;
const	FUNC  = 16;

var	Syms[SYM*SYMTBL_SIZE];
var	Nlist::NLIST_SIZE;

var	Yp, Np;

find(s) do var i;
	i := Yp-SYM;
	while (i >= 0) do
		if (str.equal(Syms[i+SNAME], s))
			return @Syms[i];
		i := i - SYM;
	end
	return 0;
end

lookup(s, f) do var y;
	y := find(s);
	if (y = 0) aw("undefined", s);
	if (y[SFLAGS] & f \= f)
		aw("unexpected type", s);
	return y;
end

newname(s) do var k, new;
	k := str.length(s)+1;
	if (Np+k >= NLIST_SIZE)
		aw("too many symbol names", s);
	new := @Nlist::Np;
	t.memcopy(s, new, k);
	Np := Np+k;
	return new;
end

add(s, f, v) do var y;
	y := find(s);
	if (y \= 0) do
		ie (y[SFLAGS] & FORW /\ f & FUNC)
			return y;
		else
			aw("redefined", s);
	end
	if (Yp+SYM >= SYMTBL_SIZE*SYM)
		aw("too many symbols", 0);
	y := @Syms[Yp];
	Yp := Yp+SYM;
	y[SNAME] := newname(s);
	y[SFLAGS] := f;
	y[SVALUE] := v;
	return y;
end

!
! Emitter
!

const	TEXT_VADDR = 134512640;		! 0x08048000
const	DATA_VADDR = TEXT_VADDR + TEXT_SIZE;

const	HEADER_SIZE = 176;	! 0xb0

const	PAGE_SIZE = 4096;

struct	RELOC = RADDR, RSEG;

var	Rel[RELOC*NRELOC];

var	Text_seg::TEXT_SIZE;
var	Data_seg::DATA_SIZE;
var	Header::HEADER_SIZE;

var	Rp, Tp, Dp, Lp, Hp;

var	Acc;

var	Codetbl;

struct	CG =	CG_ARGV, CG_INIT, CG_PUSH, CG_CLEAR,
		CG_LDVAL, CG_LDADDR, CG_LDLREF, CG_LDGLOB,
		CG_LDLOCL,
		CG_STGLOB, CG_STLOCL, CG_STINDR, CG_STINDB,
		CG_INCGLOB, CG_INCLOCL,
		CG_ALLOC, CG_DEALLOC, CG_LOCLVEC, CG_GLOBVEC,
		CG_INDEX, CG_DEREF, CG_INDXB, CG_DREFB,
		CG_MARK, CG_RESOLV,
		CG_CALL, CG_JUMPFWD, CG_JUMPBACK, CG_JMPFALSE,
		CG_JMPTRUE, CG_FOR, CG_FORDOWN,
		CG_ENTER, CG_EXIT, CG_HALT,
		CG_NEG, CG_INV, CG_LOGNOT, CG_ADD, CG_SUB,
		CG_MUL, CG_DIV, CG_MOD, CG_AND, CG_OR, CG_XOR,
		CG_SHL, CG_SHR, CG_EQ, CG_NEQ, CG_LT, CG_GT,
		CG_LE, CG_GE,
		CG_WORD;

emit8(x) do
	if (Tp >= DATA_SIZE)
		aw("text segment too big", 0);
	Text_seg::Tp := x;
	Tp := Tp+1;
end

emit32(x) do
	emit8(255&x);
	emit8(255&(x>>8));
	emit8(255&(x>>16));
	emit8(255&(x>>24));
end

emit64(x) do
	emit8(255&x);
	emit8(255&(x>>8));
	emit8(255&(x>>16));
	emit8(255&(x>>24));
	emit8(255&(x>>32));
	emit8(255&(x>>40));
	emit8(255&(x>>48));
	emit8(255&(x>>56));
end

tag(seg) do
	if (Rp+RELOC >= RELOC*NRELOC)
		oops("relocation buffer overflow", 0);
	Rel[Rp+RADDR] := seg = 't'-> Tp-BPW: Dp-BPW;
	Rel[Rp+RSEG] := seg;
	Rp := Rp+RELOC;
end

tpatch32(a, x) do
	Text_seg::a := 255&x;
	Text_seg::(a+1) := 255&(x>>8);
	Text_seg::(a+2) := 255&(x>>16);
	Text_seg::(a+3) := 255&(x>>24);
end

tpatch64(a, x) do
	Text_seg::a := 255&x;
	Text_seg::(a+1) := 255&(x>>8);
	Text_seg::(a+2) := 255&(x>>16);
	Text_seg::(a+3) := 255&(x>>24);
	Text_seg::(a+4) := 255&(x>>32);
	Text_seg::(a+5) := 255&(x>>40);
	Text_seg::(a+6) := 255&(x>>48);
	Text_seg::(a+7) := 255&(x>>56);
end

tfetch(a) return  Text_seg::a
		| (Text_seg::(a+1)<<8)
		| (Text_seg::(a+2)<<16)
		| (Text_seg::(a+3)<<24);

data8(x) do
	Data_seg::Dp := x;
	Dp := Dp+1;
end

data64(x) do
	if (Dp >= DATA_SIZE)
		aw("data segment too big", 0);
	data8(255&x);
	data8(255&(x>>8));
	data8(255&(x>>16));
	data8(255&(x>>24));
	data8(255&(x>>32));
	data8(255&(x>>40));
	data8(255&(x>>48));
	data8(255&(x>>56));
end

dpatch(a, x) do
	Data_seg::a := 255&x;
	Data_seg::(a+1) := 255&(x>>8);
	Data_seg::(a+2) := 255&(x>>16);
	Data_seg::(a+3) := 255&(x>>24);
	Data_seg::(a+4) := 255&(x>>32);
	Data_seg::(a+5) := 255&(x>>40);
	Data_seg::(a+6) := 255&(x>>48);
	Data_seg::(a+7) := 255&(x>>56);
end

dfetch(a) return  Data_seg::a
		| (Data_seg::(a+1)<<8)
		| (Data_seg::(a+2)<<16)
		| (Data_seg::(a+3)<<24)
		| (Data_seg::(a+4)<<32)
		| (Data_seg::(a+5)<<40)
		| (Data_seg::(a+6)<<48)
		| (Data_seg::(a+7)<<56);

hex(c)	ie (numeric(c))
		return c-'0';
	else
		return c-'a'+10;

rgen(s, v) do var x;
	while (s::0) do
		ie (s::0 = ',') do
			ie (s::1 = 'w') do
				emit32(v);
			end
			else ie (s::1 = 'd') do
				emit64(v);
			end
			else ie (s::1 = 'a') do
				emit64(v);
				tag('t');
			end
			else ie (s::1 = 'm') do
				push(Tp);
			end
			else ie (s::1 = '>') do
				push(Tp);
				emit32(0);
			end
			else ie (s::1 = '<') do
				emit32(pop()-Tp-BYTES_REL);
			end
			else ie (s::1 = 'r') do
				x := pop();
				tpatch32(x, Tp-x-BYTES_REL);
			end
			else do
				oops("bad code", 0);
			end
		end
		else do
			emit8(hex(s::0)*16+hex(s::1));
		end
		s := s+2;
	end
end

gen(id, v) rgen(Codetbl[id][1], v);

spill() ie (Acc)
		gen(CG_PUSH, 0);
	else
		Acc := 1;

active() return Acc;

clear() Acc := 0;

activate() Acc := 1;

relocate() do var i, a, dist;
	dist := DATA_VADDR + HEADER_SIZE + Tp;
	for (i=0, Rp, RELOC) do
		ie (Rel[i+RSEG] = 't') do
			a := tfetch(Rel[i+RADDR]);
			a := a + dist;
			tpatch64(Rel[i+RADDR], a);
		end
		else do
			a := dfetch(Rel[i+RADDR]);
			a := a + dist;
			dpatch(Rel[i+RADDR], a);
		end
	end
end

builtin(name, arity, code) do
	gen(CG_JUMPFWD, 0);
	add(name, GLOBF|FUNC | (arity << 8), Tp);
	rgen(code, 0);
	gen(CG_RESOLV, 0);
end

align(x, a) return (x+a) & ~(a-1);

hdwrite(b) do
	if (Hp >= HEADER_SIZE)
		oops("ELF header too long", 0);
	Header::Hp := b;
	Hp := Hp+1;
end

hexwrite(b)
	while (b::0) do
		hdwrite(16*hex(b::0)+hex(b::1));
		b := b+2;
	end

lewrite32(x) do
	hdwrite(x & 255);
	hdwrite(x>>8 & 255);
	hdwrite(x>>16 & 255);
	hdwrite(x>>24 & 255);
end

lewrite64(x) do
	hdwrite(x & 255);
	hdwrite(x>>8 & 255);
	hdwrite(x>>16 & 255);
	hdwrite(x>>24 & 255);
	hdwrite(x>>32 & 255);
	hdwrite(x>>40 & 255);
	hdwrite(x>>48 & 255);
	hdwrite(x>>56 & 255);
end

elfheader() do
	hexwrite("7f454c46");		! magic
	hexwrite("02");			! 64-bit
	hexwrite("01");			! little endian
	hexwrite("01");			! header version
	hexwrite("03");			! Linux ABI
	hexwrite("0000000000000000");	! padding
	hexwrite("0200");		! executable
	hexwrite("3e00");		! 386
	lewrite32(1);			! amd64
	lewrite64(TEXT_VADDR+HEADER_SIZE);! initial entry point
	lewrite64(64);			! program header offset (0x40)
	lewrite64(0);			! no header segments
	lewrite32(0);			! flags
	hexwrite("4000");		! header size
	hexwrite("3800");		! program header size
	hexwrite("0200");		! number of program headers
	hexwrite("4000");		! segment header size (unused)
	hexwrite("0000");		! number of segment headers
	hexwrite("0000");		! string index (unused)
	! text segment description
	lewrite32(1);			! loadable segment
	lewrite32(5);			! flags := read, execute
	lewrite64(HEADER_SIZE);		! offset in file
	lewrite64(TEXT_VADDR+HEADER_SIZE);! virtual load address
	lewrite64(TEXT_VADDR+HEADER_SIZE);! physical load address
	lewrite64(Tp);			! size in file
	lewrite64(Tp);			! size in memory
	lewrite64(PAGE_SIZE);		! alignment (page)
	! data segment description
	lewrite32(1);			! loadable segment
	lewrite32(6);			! flags := read, write
	lewrite64(HEADER_SIZE+Tp);	! offset in file
	lewrite64(DATA_VADDR+HEADER_SIZE+Tp);! virtual load address
	lewrite64(DATA_VADDR+HEADER_SIZE+Tp);! physical load address
	lewrite64(Dp);			! size in file
	lewrite64(Dp);			! size in memory
	lewrite64(PAGE_SIZE);		! alignment (page)
end

!
! Scanner
!

const	META	 = 256;

const	TOKEN_LEN = 128;

var	Prog::PROG_SIZE;

var	Pp, Psize;

var	T;
var	Str::TOKEN_LEN;
var	Val;
var	Oid;

var	Equal_op, Minus_op, Mul_op, Add_op;

struct	OPER = OPREC, OLEN, ONAME, OTOK, OCODE;

var	Ops;

struct	TOKENS =
	SYMBOL, INTEGER, STRING,
	ADDROF, ASSIGN, BINOP, BYTEOP, COLON, COMMA, COND,
	CONJ, DISJ, LBRACK, LPAREN, RBRACK, RPAREN, SEMI, UNOP,
	KCONST, KDECL, KDO, KELSE, KEND, KFOR, KHALT, KIE, KIF,
	KLEAVE, KLOOP, KRETURN, KSTRUCT, KVAR, KWHILE;

argparse(argc, argv) do
	if (argc \= 3) do
		log("invalid arguments\n");
		log("usage: ");
		log(argv[0]);
		log(" <t-file> <output>\n");
		halt 1;
	end
	InputFile := t.open(argv[1], O_RDONLY, 0);
	if (InputFile < 0)
		aw("couldn't open input file", 0);
	OutputFile := t.open(argv[2], O_TRUNC | O_CREAT | O_WRONLY, 493); ! 0755
	if (OutputFile < 0) do
		t.close(InputFile);
		aw("couldn't open output file", 0);
	end
end

readprog() do
	Psize := t.read(InputFile, Prog, PROG_SIZE);
	if (Psize >= PROG_SIZE)
		aw("program too big", 0);
end

readrc() do var c;
	c := Pp >= Psize-> ENDFILE: Prog::Pp;
	Pp := Pp+1;
	return c;
end

readc() do var c;
	c := readrc();
	return 'A' <= c /\ c <= 'Z'-> c-'A'+'a': c;
end

readec() do var c;
	c := readrc();
	if (c \= '\\') return c;
	c := readrc();
	if (c = 'a') return '\a';
	if (c = 'b') return '\b';
	if (c = 'e') return '\e';
	if (c = 'f') return '\f';
	if (c = 'n') return '\n';
	if (c = 'q') return '"' | META;
	if (c = 'r') return '\r';
	if (c = 's') return '\s';
	if (c = 't') return '\t';
	if (c = 'v') return '\v';
	return c;
end

reject() Pp := Pp-1;

skip() do var c;
	c := readc();
	while (1) do
		while (c = ' ' \/ c = '\t' \/ c = '\n' \/ c = '\r') do
			if (c = '\n') Line := Line+1;
			c := readc();
		end
		if (c \= '!')
			return c;
		while (c \= '\n' /\ c \= ENDFILE)
			c := readc();
	end
end

findkw(s) do
	if (s::0 = 'c') do
		if (str.equal(s, "const")) return KCONST;
		return 0;
	end
	if (s::0 = 'd') do
		if (str.equal(s, "do")) return KDO;
		if (str.equal(s, "decl")) return KDECL;
		return 0;
	end
	if (s::0 = 'e') do
		if (str.equal(s, "else")) return KELSE;
		if (str.equal(s, "end")) return KEND;
		return 0;
	end
	if (s::0 = 'f') do
		if (str.equal(s, "for")) return KFOR;
		return 0;
	end
	if (s::0 = 'h') do
		if (str.equal(s, "halt")) return KHALT;
		return 0;
	end
	if (s::0 = 'i') do
		if (str.equal(s, "if")) return KIF;
		if (str.equal(s, "ie")) return KIE;
		return 0;
	end
	if (s::0 = 'l') do
		if (str.equal(s, "leave")) return KLEAVE;
		if (str.equal(s, "loop")) return KLOOP;
		return 0;
	end
	if (s::0 = 'm') do
		if (str.equal(s, "mod")) return BINOP;
		return 0;
	end
	if (s::0 = 'r') do
		if (str.equal(s, "return")) return KRETURN;
		return 0;
	end
	if (s::0 = 's') do
		if (str.equal(s, "struct")) return KSTRUCT;
		return 0;
	end
	if (s::0 = 'v') do
		if (str.equal(s, "var")) return KVAR;
		return 0;
	end
	if (s::0 = 'w') do
		if (str.equal(s, "while")) return KWHILE;
		return 0;
	end
	return 0;
end

scanop(c) do var i, j;
	i := 0;
	j := 0;
	Oid := %1;
	while (Ops[i][OLEN] > 0) do
		ie (Ops[i][OLEN] > j) do
			if (Ops[i][ONAME]::j = c) do
				Oid := i;
				Str::j := c;
				c := readc();
				j := j+1;
			end
		end
		else do
			leave;
		end
		i := i+1;
	end
	if (Oid = %1) do
		Str::j := c;
		j := j+1;
		Str::j := 0;
		aw("unknown operator", Str);
	end
	Str::j := 0;
	reject();
	return Ops[Oid][OTOK];
end

findop(s) do var i;
	i := 0;
	while (Ops[i][OLEN] > 0) do
		if (str.equal(s, Ops[i][ONAME])) do
			Oid := i;
			return Oid;
		end
		i := i+1;
	end
	oops("operator not found", s);
end

symbolic(c) return alphabetic(c) \/ c = '_' \/ c = '.';

scan() do var c, i, k, sgn;
	c := skip();
	if (c = ENDFILE) do
		str.copy(Str, "end of file");
		return ENDFILE;
	end
	if (symbolic(c)) do
		i := 0;
		while (symbolic(c) \/ numeric(c)) do
			if (i >= TOKEN_LEN-1) do
				Str::i := 0;
				aw("symbol too long", Str);
			end
			Str::i := c;
			i := i+1;
			c := readc();
		end
		Str::i := 0;
		reject();
		k := findkw(Str);
		if (k \= 0) do
			if (k = BINOP) findop(Str);
			return k;
		end
		return SYMBOL;
	end
	if (numeric(c) \/ c = '%') do
		sgn := 1;
		i := 0;
		if (c = '%') do
			sgn := %1;
			c := readc();
			Str::i := c;
			i := i+1;
			if (\numeric(c))
				aw("missing digits after '%'", 0);
		end
		Val := 0;
		while (numeric(c)) do
			if (i >= TOKEN_LEN-1) do
				Str::i := 0;
				aw("integer too long", Str);
			end
			Str::i := c;
			i := i+1;
			Val := Val * 10 + c - '0';
			c := readc();
		end
		Str::i := 0;
		reject();
		Val := Val * sgn;
		return INTEGER;
	end
	if (c = '\'') do
		Val := readec();
		if (readc() \= '\'')
			aw("missing ''' in character", 0);
		return INTEGER;
	end
	if (c = '"') do
		i := 0;
		c := readec();
		while (c \= '"' /\ c \= ENDFILE) do
			if (i >= TOKEN_LEN-1) do
				Str::i := 0;
				aw("string too long", Str);
			end
			Str::i := c & (META-1);
			i := i+1;
			c := readec();
		end
		Str::i := 0;
		return STRING;
	end
	return scanop(c);
end

!
! Parser
!

const	MAXTBL	 = 128;
const	MAXLOOP	 = 100;

var	Fun;
var	Loop0;
var	Leaves[MAXLOOP], Lvp;
var	Loops[MAXLOOP], Llp;

expect(tok, s) do var b::100;
	if (tok = T) return;
	str.copy(b, s);
	str.append(b, " expected");
	aw(b, Str);
end

xeqsign() do
	if (T \= BINOP \/ Oid \= Equal_op)
		expect(BINOP, "'='");
	T := scan();
end

xsemi() do
	expect(SEMI, "';'");
	T := scan();
end

xlparen() do
	expect(LPAREN, "'('");
	T := scan();
end

xrparen() do
	expect(RPAREN, "')'");
	T := scan();
end

xsymbol() expect(SYMBOL, "symbol");

constfac() do var v, y;
	if (T = INTEGER) do
		v := Val;
		T := scan();
		return v;
	end
	if (T = SYMBOL) do
		y := lookup(Str, CNST);
		T := scan();
		return y[SVALUE];
	end
	aw("constant value expected", Str);
end

constval() do var v;
	v := constfac();
	ie (T = BINOP /\ Oid = Mul_op) do
		T := scan();
		v := v * constfac();
	end
	else if (T = BINOP /\ Oid = Add_op) do
		T := scan();
		v := v + constfac();
	end
	return v;
end

vardecl(glob) do var y, size;
	T := scan();
	while (1) do
		xsymbol();
		ie (glob & GLOBF)
			y := add(Str, glob, Dp);
		else
			y := add(Str, 0, Lp);
		T := scan();
		size := 1;
		ie (T = LBRACK) do
			T := scan();
			size := constval();
			if (size < 1)
				aw("invalid size", 0);
			y[SFLAGS] := y[SFLAGS] | VECT;
			expect(RBRACK, "']'");
			T := scan();
		end
		else if (T = BYTEOP) do
			T := scan();
			size := constval();
			if (size < 1)
				aw("invalid size", 0);
			size := (size + BPW-1) / BPW;
			y[SFLAGS] := y[SFLAGS] | VECT;
		end
		ie (glob & GLOBF) do
			if (y[SFLAGS] & VECT) do
				gen(CG_ALLOC, size*BPW);
				GlobSize := GlobSize + size*BPW;
				gen(CG_GLOBVEC, Dp);
			end
			data64(0);
		end
		else do
			gen(CG_ALLOC, size*BPW);
			Lp := Lp - size*BPW;
			if (y[SFLAGS] & VECT) do
				gen(CG_LOCLVEC, 0);
				Lp := Lp - BPW;
			end
			y[SVALUE] := Lp;
		end
		if (T \= COMMA) leave;
		T := scan();
	end
	xsemi();
end

constdecl(glob) do var y;
	T := scan();
	while (1) do
		xsymbol();
		y := add(Str, glob|CNST, 0);
		T := scan();
		xeqsign();
		y[SVALUE] := constval();
		if (T \= COMMA) leave;
		T := scan();
	end
	xsemi();
end

stcdecl(glob) do var y, i;
	T := scan();
	xsymbol();
	y := add(Str, glob|CNST, 0);
	T := scan();
	xeqsign();
	i := 0;
	while (1) do
		xsymbol();
		add(Str, glob|CNST, i);
		i := i+1;
		T := scan();
		if (T \= COMMA) leave;
		T := scan();
	end
	y[SVALUE] := i;
	xsemi();
end

fwddecl() do var y, n;
	T := scan();
	while (1) do
		xsymbol();
		y := add(Str, GLOBF|FORW, 0);
		T := scan();
		xlparen();
		n := constval();
		if (n < 0) aw("invalid arity", 0);
		y[SFLAGS] := y[SFLAGS] | (n << 8);
		xrparen();
		if (T \= COMMA) leave;
		T := scan();
	end
	xsemi();
end

resolve_fwd(loc, fn) do var nloc;
	while (loc \= 0) do
		nloc := tfetch(loc);
		tpatch32(loc, fn-loc-BYTES_REL);
		loc := nloc;
	end
end

decl	compound(0), stmt(0);

fundecl() do
	var	l_base, l_addr;
	var	i, na, oyp, onp;
	var	y;

	l_addr := 0;
	na := 0;
	gen(CG_JUMPFWD, 0);
	y := add(Str, GLOBF|FUNC, Tp);
	T := scan();
	xlparen();
	oyp := Yp;
	onp := Np;
	l_base := Yp;
	while (T = SYMBOL) do
		add(Str, 0, l_addr);
		l_addr := l_addr + BPW;
		na := na+1;
		T := scan();
		if (T \= COMMA) leave;
		T := scan();
	end
	for (i = l_base, Yp, SYM) do
		! na+1 because of the return address on the stack
		Syms[i+SVALUE] := (na+1)*BPW - Syms[i+SVALUE];
	end
	if (y[SFLAGS] & FORW) do
		resolve_fwd(y[SVALUE], Tp);
		if (na \= y[SFLAGS] >> 8)
			aw("function does not match DECL", y[SNAME]);
		y[SFLAGS] := y[SFLAGS] & ~FORW;
		y[SFLAGS] := y[SFLAGS] | FUNC;
		y[SVALUE] := Tp;
	end
	xrparen();
	y[SFLAGS] := y[SFLAGS] | (na << 8);
	gen(CG_ENTER, 0);
	Fun := 1;
	stmt();
	Fun := 0;
	gen(CG_CLEAR, 0);
	gen(CG_EXIT, 0);
	gen(CG_RESOLV, 0);
	Yp := oyp;
	Np := onp;
	Lp := 0;
end

declaration(glob)
	ie (T = KVAR)
		vardecl(glob);
	else ie (T = KCONST)
		constdecl(glob);
	else ie (T = KSTRUCT)
		stcdecl(glob);
	else ie (T = KDECL)
		fwddecl();
	else
		fundecl();

decl	expr(1);

fncall(fn) do var i;
	T := scan();
	if (fn = 0) aw("call of non-function", 0);
	if (fn[SFLAGS] & (FUNC|FORW) = 0)
		aw("call of non-function", fn[SNAME]);
	i := 0;
	while (T \= RPAREN) do
		expr(0);
		i := i+1;
		if (T \= COMMA) leave;
		T := scan();
		if (T = RPAREN)
			aw("syntax error", Str);
	end
	if (i \= fn[SFLAGS] >> 8)
		aw("wrong number of arguments", fn[SNAME]);
	expect(RPAREN, "')'");
	T := scan();
	if (active())
		spill();
	ie (fn[SFLAGS] & FORW) do
		gen(CG_CALL, fn[SVALUE]);
		fn[SVALUE] := Tp-BYTES_REL;
	end
	else do
		gen(CG_CALL, fn[SVALUE]-Tp-(BYTES_REL+1));
	end
	if (i \= 0) gen(CG_DEALLOC, i*BPW);
	activate();
end

mkstring(s) do var i, a, k;
	a := Dp;
	k := str.length(s);
	for (i=0, k+1)
		data8(s::i);
	while (Dp mod BPW \= 0)
		data8(0);
	return a;
end

mktable() do
	var	n, i, a;
	var	tbl[MAXTBL], af[MAXTBL];
	var	dynamic;

	T := scan();
	dynamic := 0;
	n := 0;
	while (T \= RBRACK) do
		if (n >= MAXTBL)
			aw("table too big", 0);
		ie (T = LPAREN /\ \dynamic) do
			T := scan();
			dynamic := 1;
			loop;
		end
		else ie (dynamic) do
			expr(1);
			gen(CG_STGLOB, 0);
			tbl[n] := 0;
			af[n] := Tp-BPW;
			n := n+1;
			if (T = RPAREN) do
				T := scan();
				dynamic := 0;
			end
		end
		else ie (T = INTEGER \/ T = SYMBOL) do
			tbl[n] := constval();
			af[n] := 0;
			n := n+1;
		end
		else ie (T = STRING) do
			tbl[n] := mkstring(Str);
			af[n] := 1;
			n := n+1;
			T := scan();
		end
		else ie (T = LBRACK) do
			tbl[n] := mktable();
			af[n] := 1;
			n := n+1;
		end
		else do
			aw("invalid table element", Str);
		end
		if (T \= COMMA) leave;
		T := scan();
		if (T = RBRACK)
			aw("syntax error", Str);
	end
	if (dynamic)
		aw("missing ')' in dynamic table", 0);
	expect(RBRACK, "']'");
	if (n = 0) aw("empty table", 0);
	T := scan();
	a := Dp;
	for (i=0, n) do
		data64(tbl[i]);
		ie (af[i] = 1) do
			tag('d');
		end
		else if (af[i] > 1) do
			tpatch64(af[i], Dp-BPW);
		end
	end
	return a;
end

load(y) ie (y[SFLAGS] & GLOBF)
		gen(CG_LDGLOB, y[SVALUE]);
	else
		gen(CG_LDLOCL, y[SVALUE]);

store(y)
	ie (y[SFLAGS] & GLOBF)
		gen(CG_STGLOB, y[SVALUE]);
	else
		gen(CG_STLOCL, y[SVALUE]);

decl	factor(0);

address(lv, bp) do var y;
	y := lookup(Str, 0);
	T := scan();
	ie (y[SFLAGS] & CNST) do
		if (lv > 0) aw("invalid location", y[SNAME]);
		spill();
		gen(CG_LDVAL, y[SVALUE]);
	end
	else ie (y[SFLAGS] & (FUNC|FORW)) do
		if (lv = 2) aw("invalid location", y[SNAME]);
	end
	else if (lv = 0 \/ T = LBRACK \/ T = BYTEOP) do
		spill();
		load(y);
	end
	if (T = LBRACK \/ T = BYTEOP)
		if (y[SFLAGS] & (FUNC|FORW|CNST))
			aw("bad subscript", y[SNAME]);
	while (T = LBRACK) do
		T := scan();
		bp[0] := 0;
		expr(0);
		expect(RBRACK, "']'");
		T := scan();
		y := 0;
		gen(CG_INDEX, 0);
		if (lv = 0 \/ T = LBRACK  \/ T = BYTEOP)
			gen(CG_DEREF, 0);
	end
	if (T = BYTEOP) do
		T := scan();
		bp[0] := 1;
		factor();
		y := 0;
		gen(CG_INDXB, 0);
		if (lv = 0) gen(CG_DREFB, 0);
	end
	return y;
end

factor() do var y, op, b;
	ie (T = INTEGER) do
		spill();
		gen(CG_LDVAL, Val);
		T := scan();
	end
	else ie (T = SYMBOL) do
		y := address(0, @b);
		if (T = LPAREN) fncall(y);
	end
	else ie (T = STRING) do
		spill();
		gen(CG_LDADDR, mkstring(Str));
		T := scan();
	end
	else ie (T = LBRACK) do
		spill();
		gen(CG_LDADDR, mktable());
	end
	else ie (T = ADDROF) do
		T := scan();
		y := address(2, @b);
		ie (y = 0) do
			;
		end
		else ie (y[SFLAGS] & GLOBF) do
			spill();
			gen(CG_LDADDR, y[SVALUE]);
		end
		else do
			spill();
			gen(CG_LDLREF, y[SVALUE]);
		end
	end
	else ie (T = BINOP) do
		if (Oid \= Minus_op)
			aw("syntax error", Str);
		T := scan();
		factor();
		gen(CG_NEG, 0);
	end
	else ie (T = UNOP) do
		op := Oid;
		T := scan();
		factor();
		gen(Ops[op][OCODE], 0);
	end
	else ie (T = LPAREN) do
		T := scan();
		expr(0);
		xrparen();
	end
	else do
		aw("syntax error", Str);
	end
end

emitop(stk, p) do
	gen(Ops[stk[p-1]][OCODE], 0);
	return p-1;
end

arith() do var stk[10], p;
	factor();
	p := 0;
	while (T = BINOP) do
		while (p /\ Ops[Oid][OPREC] <= Ops[stk[p-1]][OPREC])
			p := emitop(stk, p);
		stk[p] := Oid;
		p := p+1;
		T := scan();
		factor();
	end
	while (p > 0)
		p := emitop(stk, p);
end

conjn() do var n;
	arith();
	n := 0;
	while (T = CONJ) do
		T := scan();
		gen(CG_JMPFALSE, 0);
		clear();
		arith();
		n := n+1;
	end
	while (n > 0) do
		gen(CG_RESOLV, 0);
		n := n-1;
	end
end

disjn() do var n;
	conjn();
	n := 0;
	while (T = DISJ) do
		T := scan();
		gen(CG_JMPTRUE, 0);
		clear();
		conjn();
		n := n+1;
	end
	while (n > 0) do
		gen(CG_RESOLV, 0);
		n := n-1;
	end
end

expr(clr) do
	if (clr) clear();
	disjn();
	if (T = COND) do
		T := scan();
		gen(CG_JMPFALSE, 0);
		expr(1);
		expect(COLON, "':'");
		T := scan();
		gen(CG_JUMPFWD, 0);
		swap();
		gen(CG_RESOLV, 0);
		expr(1);
		gen(CG_RESOLV, 0);
	end
end

halt_stmt() do
	T := scan();
	gen(CG_HALT, constval());
	xsemi();
end

return_stmt() do
	T := scan();
	if (Fun = 0)
		aw("can't return from main body", 0);
	ie (T = SEMI)
		gen(CG_CLEAR, 0);
	else
		expr(1);
	if (Lp \= 0) do
		gen(CG_DEALLOC, -Lp);
	end
	gen(CG_EXIT, 0);
	xsemi();
end

if_stmt(alt) do
	T := scan();
	xlparen();
	expr(1);
	gen(CG_JMPFALSE, 0);
	xrparen();
	stmt();
	if (alt) do
		gen(CG_JUMPFWD, 0);
		swap();
		gen(CG_RESOLV, 0);
		expect(KELSE, "ELSE");
		T := scan();
		stmt();
	end
	gen(CG_RESOLV, 0);
end

while_stmt() do var olp, olv;
	T := scan();
	olp := Loop0;
	olv := Lvp;
	gen(CG_MARK, 0);
	Loop0 := tos();
	xlparen();
	expr(1);
	xrparen();
	gen(CG_JMPFALSE, 0);
	stmt();
	swap();
	gen(CG_JUMPBACK, 0);
	gen(CG_RESOLV, 0);
	while (Lvp > olv) do
		push(Leaves[Lvp-1]);
		gen(CG_RESOLV, 0);
		Lvp := Lvp-1;
	end
	Loop0 := olp;
end

for_stmt() do
	var	y;
	var	step;
	var	oll, olp, olv;
	var	test;

	T := scan();
	oll := Llp;
	olv := Lvp;
	olp := Loop0;
	Loop0 := 0;
	xlparen();
	xsymbol();
	y := lookup(Str, 0);
	if (y[SFLAGS] & (CNST|FUNC|FORW))
		aw("unexpected type", y[SNAME]);
	T := scan();
	xeqsign();
	expr(1);
	store(y);
	expect(COMMA, "','");
	T := scan();
	gen(CG_MARK, 0);
	test := tos();
	load(y);
	expr(0);
	ie (T = COMMA) do
		T := scan();
		step := constval();
	end
	else do
		step := 1;
	end
	gen(step<0-> CG_FORDOWN: CG_FOR, 0);
	xrparen();
	stmt();
	while (Llp > oll) do
		push(Loops[Llp-1]);
		gen(CG_RESOLV, 0);
		Llp := Llp-1;
	end
	ie (y[SFLAGS] & GLOBF)
		gen(CG_INCGLOB, y[SVALUE]);
	else
		gen(CG_INCLOCL, y[SVALUE]);
	gen(CG_WORD, step);
	swap();
	gen(CG_JUMPBACK, 0);
	gen(CG_RESOLV, 0);
	while (Lvp > olv) do
		push(Leaves[Lvp-1]);
		gen(CG_RESOLV, 0);
		Lvp := Lvp-1;
	end
	Loop0 := olp;
end

leave_stmt() do
	T := scan();
	if (Loop0 < 0)
		aw("LEAVE not in loop context", 0);
	xsemi();
	if (Lvp >= MAXLOOP)
		aw("too many LEAVEs", 0);
	gen(CG_JUMPFWD, 0);
	Leaves[Lvp] := pop();
	Lvp := Lvp+1;
end

loop_stmt() do
	T := scan();
	if (Loop0 < 0)
		aw("LOOP not in loop context", 0);
	xsemi();
	ie (Loop0 > 0) do
		push(Loop0);
		gen(CG_JUMPBACK, 0);
	end
	else do
		if (Llp >= MAXLOOP)
			aw("too many LOOPs", 0);
		gen(CG_JUMPFWD, 0);
		Loops[Llp] := pop();
		Llp := Llp+1;
	end
end

asg_or_call() do var y, b;
	clear();
	y := address(1, @b);
	ie (T = LPAREN) do
		fncall(y);
	end
	else ie (T = ASSIGN) do
		T := scan();
		expr(0);
		ie (y = 0)
			gen(b-> CG_STINDB: CG_STINDR, 0);
		else ie (y[SFLAGS] & (FUNC|FORW|CNST|VECT))
			aw("bad location", y[SNAME]);
		else
			store(y);
	end
	else do
		aw("syntax error", Str);
	end
	xsemi();
end

stmt() ie (T = KFOR)
		for_stmt();
	else ie (T = KHALT)
		halt_stmt();
	else ie (T = KIE)
		if_stmt(1);
	else ie (T = KIF)
		if_stmt(0);
	else ie (T = KELSE)
		aw("ELSE without IE", 0);
	else ie (T = KLEAVE)
		leave_stmt();
	else ie (T = KLOOP)
		loop_stmt();
	else ie (T = KRETURN)
		return_stmt();
	else ie (T = KWHILE)
		while_stmt();
	else ie (T = KDO)
		compound();
	else ie (T = SYMBOL)
		asg_or_call();
	else ie (T = SEMI)
		T := scan();
	else
		expect(%1, "statement");

compound() do var oyp, olp, onp, msg;
	msg := "unexpected end of compound statement";
	T := scan();
	oyp := Yp;
	onp := Np;
	olp := Lp;
	while (T = KVAR \/ T = KCONST \/ T = KSTRUCT)
		declaration(0);
	while (T \= KEND) do
		if (T = ENDFILE) aw(msg, 0);
		stmt();
	end
	T := scan();
	if (olp-Lp \= 0)
		gen(CG_DEALLOC, olp-Lp);
	Yp := oyp;
	Np := onp;
	Lp := olp;
end

program() do var i;
	gen(CG_ARGV, 0);

	T := scan();
	while (	T = KVAR \/ T = KCONST \/ T = SYMBOL \/
		T = KDECL \/ T = KSTRUCT
	)
		declaration(GLOBF);
	if (T \= KDO)
		aw("DO or declaration expected", 0);
	gen(CG_INIT, 0);
	add("argv", VECT, GlobSize);
	add("argc", 0, GlobSize+BPW);
	compound();
	if (T \= ENDFILE)
		aw("trailing characters", Str);
	gen(CG_HALT, 0);
	for (i=0, Yp, SYM)
		if (Syms[i+SFLAGS] & FORW /\ Syms[i+SVALUE])
			aw("undefined function", Syms[i+SNAME]);
end

!
! Main
!

init() do
	var i, tread, twrite, tcomp, tcopy, tfill, tscan, tpopcnt, tgetrandom,
	    topen, tclose;
	Rp := 0;
	Tp := 0;
	Dp := 0;
	Lp := 0;
	Sp := 0;
	Yp := 0;
	Np := 0;
	Pp := 0;
	Hp := 0;
	Line := 1;
	GlobSize := 0;
	Acc := 0;
	Fun := 0;
	Loop0 := %1;
	Lvp := 0;
	Llp := 0;
	Codetbl := [
		[ CG_ARGV,	"488d4c240851"		],
		[ CG_INIT,	"4889e5"		],
		[ CG_PUSH,	"50"			],
		[ CG_CLEAR,	"4831c0"		],
		[ CG_LDVAL,	"48b8,d"		],
		[ CG_LDADDR,	"48b8,a"		],
		[ CG_LDLREF,	"488d85,w"		],
		[ CG_LDGLOB,	"48a1,a"		],
		[ CG_LDLOCL,	"488b85,w"		],
		[ CG_STGLOB,	"48a3,a"		],
		[ CG_STLOCL,	"488985,w"		],
		[ CG_STINDR,	"5b488903"		],
		[ CG_STINDB,	"5b8803"		],
		[ CG_INCGLOB,	"48810425,a"		],
		[ CG_INCLOCL,	"488185,w"		],
		[ CG_ALLOC,	"4881ec,w"		],
		[ CG_DEALLOC,	"4881c4,w"		],
		[ CG_LOCLVEC,	"4889e050"		],
		[ CG_GLOBVEC,	"48b9,a488921"		],
		[ CG_INDEX,	"48c1e0035b4801d8"	],
		[ CG_DEREF,	"488b00"		],
		[ CG_INDXB,	"5b4801d8"		],
		[ CG_DREFB,	"4889c34831c08a03"	],
		[ CG_MARK,	",m"			],
		[ CG_RESOLV,	",r"			],
		[ CG_CALL,	"e8,w"			],
		[ CG_JUMPFWD,	"e9,>"			],
		[ CG_JUMPBACK,	"e9,<"			],
		[ CG_JMPFALSE,	"4809c00f84,>"		],
		[ CG_JMPTRUE,	"4809c00f85,>"		],
		[ CG_FOR,	"5b4839c30f8d,>"	],
		[ CG_FORDOWN,	"5b4839c30f8e,>"	],
		[ CG_ENTER,	"554889e5"		],
		[ CG_EXIT,	"5dc3"			],
		[ CG_HALT,	"b83c000000bf,w0f05"	],
		[ CG_NEG,	"48f7d8"		],
		[ CG_INV,	"48f7d0"		],
		[ CG_LOGNOT,	"48f7d84819c048f7d0"	],
		[ CG_ADD,	"5b4801d8"		],
		[ CG_SUB,	"4889c3584829d8"	],
		[ CG_MUL,	"5b48f7eb"		],
		[ CG_DIV,	"4889c358489948f7fb"	],
		[ CG_MOD,	"4889c358489948f7fb4889d0"],
		[ CG_AND,	"5b4821d8"		],
		[ CG_OR,	"5b4809d8"		],
		[ CG_XOR,	"5b4831d8"		],
		[ CG_SHL,	"4889c15848d3e0"	],
		[ CG_SHR,	"4889c15848d3e8"	],
		[ CG_EQ,	"5b4839c30f95c2480fb6c248ffc8"	],
		[ CG_NEQ,	"5b4839c30f94c2480fb6c248ffc8"	],
		[ CG_LT,	"5b4839c30f9dc2480fb6c248ffc8"	],
		[ CG_GT,	"5b4839c30f9ec2480fb6c248ffc8"	],
		[ CG_LE,	"5b4839c30f9fc2480fb6c248ffc8"	],
		[ CG_GE,	"5b4839c30f9cc2480fb6c248ffc8"	],
		[ CG_WORD,	",w"			],
		[ %1,		""			] ];
	tread := "4831c0488b7c2418488b742410488b5424080f05c3";
	twrite := "4831c048ffc0488b7c2418488b742410488b5424080f05c3";
	tcomp :=
  "488b742418488b7c2410488b4c240848ffc1fcf3a64809c90f85040000004831c0c38a46ff2a47ff66986699c3";
	tcopy := "488b742418488b7c2410488b4c2408fcf3a4c3";
	tfill := "488b7c2418488b442410488b4c2408fcf3aac3";
	tscan :=
  "488b7c2418488b442410488b4c240848ffc14889fafcf2ae4809c90f840a0000004889f84829d048ffc8c34831c048ffc8c3";
	tpopcnt := "f3480fb8442408c3";
	! 00000000  48B83E0100000000  mov rax,0x13e
	!	   -0000
	! 0000000A  488B7C2418	      mov rdi,[rsp+0x18]
	! 0000000F  488B742410	      mov rsi,[rsp+0x10]
	! 00000014  488B542408	      mov rdx,[rsp+0x8]
	! 00000019  0F05	      syscall
	! 0000001B  C3		      ret
        tgetrandom := "48b83e01000000000000488b7c2418488b742410488b5424080f05c3";
        topen := "b802000000488b7c2418488b742410488b5424080f05c3";
        tclose := "b803000000488b7c24080f05c3";
	Ops := [[ 7, 3, "mod",	BINOP,  CG_MOD		],
		[ 6, 1, "+",	BINOP,  CG_ADD		],
		[ 7, 1, "*",	BINOP,  CG_MUL		],
		[ 0, 1, ";",	SEMI,   0		],
		[ 0, 1, ",",	COMMA,  0		],
		[ 0, 1, "(",	LPAREN, 0		],
		[ 0, 1, ")",	RPAREN, 0		],
		[ 0, 1, "[",	LBRACK, 0		],
		[ 0, 1, "]",	RBRACK, 0		],
		[ 3, 1, "=",	BINOP,  CG_EQ		],
		[ 5, 1, "&",	BINOP,  CG_AND		],
		[ 5, 1, "|",	BINOP,  CG_OR		],
		[ 5, 1, "^",	BINOP,  CG_XOR		],
		[ 0, 1, "@",	ADDROF, 0		],
		[ 0, 1, "~",	UNOP,   CG_INV		],
		[ 0, 1, ":",	COLON,  0		],
		[ 0, 2, "::",	BYTEOP, 0		],
		[ 0, 2, ":=",	ASSIGN, 0		],
		[ 0, 1, "\\",	UNOP,   CG_LOGNOT	],
		[ 1, 2, "\\/",	DISJ,   0		],
		[ 3, 2, "\\=",	BINOP,  CG_NEQ		],
		[ 4, 1, "<",	BINOP,  CG_LT		],
		[ 4, 2, "<=",	BINOP,  CG_LE		],
		[ 5, 2, "<<",	BINOP,  CG_SHL		],
		[ 4, 1, ">",	BINOP,  CG_GT		],
		[ 4, 2, ">=",   BINOP,  CG_GE		],
		[ 5, 2, ">>",	BINOP,  CG_SHR		],
		[ 6, 1, "-",	BINOP,  CG_SUB		],
		[ 0, 2, "->",	COND,   0		],
		[ 7, 1, "/",	BINOP,  CG_DIV		],
		[ 2, 2, "/\\",	CONJ,   0		],
		[ 0, 0, 0,	0,      0		] ];
	Equal_op := findop("=");
	Minus_op := findop("-");
	Mul_op := findop("*");
	Add_op := findop("+");
	i := 0;
	while (Codetbl[i][0] \= %1) do
		if (Codetbl[i][0] \= i)
			oops("bad code table entry", ntoa(i));
		i := i+1;
	end
	builtin("t.read", 3, tread);
	builtin("t.write", 3, twrite);
	builtin("t.memcomp", 3, tcomp);
	builtin("t.memcopy", 3, tcopy);
	builtin("t.memfill", 3, tfill);
	builtin("t.memscan", 3, tscan);
	builtin("t.popcnt", 1, tpopcnt);
	builtin("t.getrandom", 3, tgetrandom);
	builtin("t.open", 3, topen);
	builtin("t.close", 1, tclose);
end

do
	init();
	argparse(argc, argv);
	readprog();
	program();
	! 16-byte align in file
	Tp := align(HEADER_SIZE+Tp, 16)-HEADER_SIZE;
	relocate();
	elfheader();
	t.write(OutputFile, Header, Hp);
	t.write(OutputFile, Text_seg, Tp);
	t.write(OutputFile, Data_seg, Dp);
	t.close(InputFile);
	t.close(OutputFile);
end

! Local Variables:
! indent-tabs-mode: t
! tab-width: 8
! End:
