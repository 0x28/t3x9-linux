CFLAGS += -Wall -Wextra -Wconversion -static

.PHONY: all clean test
.DELETE_ON_ERROR:

all:	t.elf

t.elf:	test
	cp t3 t.elf

test:	t2 t3
	cmp t2 t3

t0:	t.c
	$(CC) $(CFLAGS) $< -o $@

t1:	t0 t.t
	./t0 t.t t1

t2:	t1 t.t
	./t1 t.t t2

t3:	t2 t.t
	./t2 t.t t3

clean:
	$(RM) t0 t1 t2 t3 a.out dump *.o *.core
