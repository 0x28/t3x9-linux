/*
 * T3X9 -> ELF-FreeBSD-386 compiler
 * Nils M Holm, 2017, CC0 license
 * https://creativecommons.org/publicdomain/zero/1.0/
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>

#define BPW	8
#define BYTES_REL 4

#define PROG_SIZE	0x10000

#define TEXT_VADDR	0x08048000
#define DATA_VADDR	0x08058000

#define TEXT_SIZE	0x10000
#define DATA_SIZE	0x10000

#define NRELOC		10000

#define STACK_SIZE	100

#define SYMTBL_SIZE	1000
#define NLIST_SIZE	10000

#define byte	unsigned char

int64_t	Stk[STACK_SIZE], Sp = 0;

uint64_t Line = 1;
int64_t GlobSize = 0;

int32_t InputFile = 0;
int32_t OutputFile = 0;


void aw(char *m, char *s) {
	fprintf(stderr, "t3x9: %ld: %s", Line, m);
	if (s != NULL)
		fprintf(stderr, ": %s", s);
	fputc('\n', stderr);
	exit(1);
}

void oops(char *m, char *s) {
	fprintf(stderr, "t3x9: internal error\n");
	aw(m, s);
}

void push(int64_t x) {
	if (Sp >= STACK_SIZE)
		aw("too many nesting levels", NULL);
	Stk[Sp++] = x;
}

int64_t tos(void) {
	return Stk[Sp-1];
}

int64_t pop(void) {
	if (Sp < 1)
		oops("stack underflow", NULL);
	return Stk[--Sp];
}

void swap(void) {
	int64_t	t;

	if (Sp < 2)
		oops("stack underflow", NULL);
	t = Stk[Sp-1];
	Stk[Sp-1] = Stk[Sp-2];
	Stk[Sp-2] = t;
}

/*
 * Symbol table
 */

struct _symbol {
	char	*name;
	int64_t	flags;
	int64_t value;
};

#define sym	struct _symbol

#define GLOBF	1
#define CNST	2
#define VECT	4
#define DECL	8
#define FUNC	16

sym	Sym[SYMTBL_SIZE];
char	Nlist[NLIST_SIZE];

int64_t	Yp = 0, Np = 0;

sym *find(char *s) {
	int64_t	i;

	for (i=Yp-1; i>=0; i--) {
		if (!strcmp(Sym[i].name, s))
			return &Sym[i];
	}
	return NULL;
}

sym *lookup(char *s, uint32_t f) {
	sym	*y;

	y = find(s);
	if (NULL == y)
		aw("undefined", s);
	if ((y->flags & f) != f)
		aw("unexpected type", s);
	return y;
}

sym *add(char *s, uint32_t f, int64_t v) {
	sym	*y;

	y = find(s);
	if (y != NULL && (y->flags & GLOBF) == (f & GLOBF)) {
		if (y->flags & DECL && f & FUNC)
			return y;
		else
			aw("redefined", s);
	}
	if (Yp >= SYMTBL_SIZE)
		aw("too many symbols", NULL);
	Sym[Yp].name = strdup(s);
	Sym[Yp].flags = f;
	Sym[Yp].value = v;
	Yp++;
	return &Sym[Yp-1];
}

/*
 * Emitter
 */

#define HEADER_SIZE	0xb0
#define PAGE_SIZE	0x1000

struct _reloc {
	int64_t	addr;
	char	seg;
};

#define reloc	struct _reloc

reloc	Rel[NRELOC];

byte	Text[TEXT_SIZE];
byte	Data[DATA_SIZE];

int64_t Rp = 0, Tp = 0, Dp = 0, Lp = 0;

uint8_t 	Loaded = 0;

// stack after CG_INIT:
// ...
// argv[1]
// argv[0]
// argc
// pointer to argv[0] == local vector
// global vectors ...
// rbp points here

// 00000000  488D4C2408        lea rcx,[rsp+0x8]
// 00000005  51                push rcx
#define CG_ARGV		"488d4c240851"

// 00000000  4889E5            mov rbp,rsp
#define CG_INIT     "4889e5"

// 00000000  50                push rax
#define CG_PUSH		"50"

// 00000000  48B8000000000000  mov rax,w
//          -0000
#define CG_LDVAL	"48b8,d"

// 00000000  48B8000000000000  mov rax,a
//          -0000
#define CG_LDADDR	"48b8,a"

// 00000000  488D8500000000    lea rax,[rbp+w]
#define CG_LDLREF	"488d85,w"

// 00000000  48A1000000000000  mov rax,[qword a]
//          -0000
#define CG_LDGLOB	"48a1,a"

// 00000000  488B8500000000    mov rax,[rbp+w]
#define CG_LDLOCL	"488b85,w"

// 00000000  4831C0            xor rax,rax
#define CG_CLEAR	"4831c0"

// 00000000  48A3000000000000  mov [qword a],rax
//          -0000
#define CG_STGLOB	"48a3,a"

// 00000000  48898500000000    mov [rbp+w],rax
#define CG_STLOCL	"488985,w"

// 00000000  5B                pop rbx
// 00000001  488903            mov [rbx],rax
#define CG_STINDR	"5b488903"

// 00000000  5B                pop rbx
// 00000001  8803              mov [rbx],al
#define CG_STINDB	"5b8803"

// 00000000  4881EC00000000    sub rsp,w
#define CG_ALLOC	"4881ec,w"

// 00000000  4881C400000000    add rsp,w
#define CG_DEALLOC	"4881c4,w"

// 00000000  4889E0            mov rax,rsp
// 00000003  50                push rax
#define CG_LOCLVEC	"4889e050"

// 00000000  48B9000000000000  mov rcx,a
//          -0000
// 0000000A  488921            mov [rcx],rsp
#define CG_GLOBVEC	"48b9,a488921"

// 00000000  B83C000000        mov eax,0x3c
// 00000005  BF00000000        mov edi,w
// 0000000A  0F05              syscall
#define CG_HALT		"b83c000000bf,w0f05" // 32 bit only

// 00000000  48C1E003          shl rax,byte 0x3
// 00000004  5B                pop rbx
// 00000005  4801D8            add rax,rbx
#define CG_INDEX	"48c1e0035b4801d8"

// 00000000  488B00            mov rax,[rax]
#define CG_DEREF	"488b00"

// 00000000  5B                pop rbx
// 00000001  4801D8            add rax,rbx
#define CG_INDXB	"5b4801d8"

// 00000000  4889C3            mov rbx,rax
// 00000003  4831C0            xor rax,rax
// 00000006  8A03              mov al,[rbx]
#define CG_DREFB	"4889c34831c08a03"

// 00000000  E800000000        call w
#define CG_CALL		"e8,w"

#define CG_MARK		",m"

// 00000000  E900000000        jmp >
#define CG_JUMPFWD	"e9,>"

// 00000000  E900000000        jmp <
#define CG_JUMPBACK	"e9,<"

// 00000000  55                push rbp
// 00000001  4889E5            mov rbp,rsp
#define CG_ENTER	"554889e5"

// 00000000  5D                pop rbp
// 00000001  C3                ret
#define CG_EXIT		"5dc3"

#define CG_RESOLV	",r"

// 00000000  48F7D8            neg rax
#define CG_NEG		"48f7d8"

// 00000000  48F7D0            not rax
#define CG_INV		"48f7d0"

// 00000000  48F7D8            neg rax
// 00000003  4819C0            sbb rax,rax
// 00000006  48F7D0            not rax
#define CG_LOGNOT	"48f7d84819c048f7d0"

// 00000000  5B                pop rbx
// 00000001  4801D8            add rax,rbx
#define CG_ADD		"5b4801d8"

// 00000000  4889C3            mov rbx,rax
// 00000003  58                pop rax
// 00000004  4829D8            sub rax,rbx
#define CG_SUB		"4889c3584829d8"

// 00000000  5B                pop rbx
// 00000001  48F7EB            imul rbx
#define CG_MUL		"5b48f7eb"

// 00000000  4889C3            mov rbx,rax
// 00000003  58                pop rax
// 00000004  4899              cqo
// 00000006  48F7FB            idiv rbx
#define CG_DIV		"4889c358489948f7fb"

// 00000000  4889C3            mov rbx,rax
// 00000003  58                pop rax
// 00000004  4899              cqo
// 00000006  48F7FB            idiv rbx
// 00000009  4889D0            mov rax,rdx
#define CG_MOD		"4889c358489948f7fb4889d0"

// 00000000  5B                pop rbx
// 00000001  4821D8            and rax,rbx
#define CG_AND		"5b4821d8"

// 00000000  5B                pop rbx
// 00000001  4809D8            or rax,rbx
#define CG_OR		"5b4809d8"

// 00000000  5B                pop rbx
// 00000001  4831D8            xor rax,rbx
#define CG_XOR		"5b4831d8"

// 00000000  4889C1            mov rcx,rax
// 00000003  58                pop rax
// 00000004  48D3E0            shl rax,cl
#define CG_SHL		"4889c15848d3e0"

// 00000000  4889C1            mov rcx,rax
// 00000003  58                pop rax
// 00000004  48D3E8            shr rax,cl
#define CG_SHR		"4889c15848d3e8"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F95C2            setnz dl
// 00000007  480FB6C2          movzx rax,dl
// 0000000B  48FFC8            dec rax
#define CG_EQ		"5b4839c30f95c2480fb6c248ffc8"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F94C2            setz dl
// 00000007  480FB6C2          movzx rax,dl
// 0000000B  48FFC8            dec rax
#define CG_NEQ		"5b4839c30f94c2480fb6c248ffc8"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F9DC2            setnl dl
// 00000007  480FB6C2          movzx rax,dl
// 0000000B  48FFC8            dec rax
#define CG_LT		"5b4839c30f9dc2480fb6c248ffc8"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F9EC2            setng dl
// 00000007  480FB6C2          movzx rax,dl
// 0000000B  48FFC8            dec rax
#define CG_GT		"5b4839c30f9ec2480fb6c248ffc8"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F9FC2            setg dl
// 00000007  480FB6C2          movzx rax,dl
// 0000000B  48FFC8            dec rax
#define CG_LE		"5b4839c30f9fc2480fb6c248ffc8"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F9CC2            setl dl
// 00000007  480FB6C2          movzx rax,dl
// 0000000B  48FFC8            dec rax
#define CG_GE		"5b4839c30f9cc2480fb6c248ffc8"

// 00000000  4809C0            or rax,rax
// 00000003  0F8400000000      jz near >
#define CG_JMPFALSE	"4809c00f84,>"

// 00000000  4809C0            or rax,rax
// 00000003  0F8500000000      jnz near >
#define CG_JMPTRUE	"4809c00f85,>"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F8D00000000      jnl near >
#define CG_FOR		"5b4839c30f8d,>"

// 00000000  5B                pop rbx
// 00000001  4839C3            cmp rbx,rax
// 00000004  0F8E00000000      jng near >
#define CG_FORDOWN	"5b4839c30f8e,>"

// 00000000  4881042500000000  add qword [w],v
//          -00000000
#define CG_INCGLOB	"48810425,a" // NOTE: word follows

// 00000000  4881850000000000  add qword [rbp+w],v
//          -000000
#define CG_INCLOCL	"488185,w"   // NOTE: word follows

#define CG_WORD		",w"

// t.read:
// 00000000  4831C0            xor rax,rax
// 00000003  488B7C2418        mov rdi,[rsp+0x18]
// 00000008  488B742410        mov rsi,[rsp+0x10]
// 0000000D  488B542408        mov rdx,[rsp+0x8]
// 00000012  0F05              syscall
// 00000014  C3                ret
#define CG_P_READ \
	"4831c0488b7c2418488b742410488b5424080f05c3"

// t.write:
// 00000000  4831C0            xor rax,rax
// 00000003  48FFC0            inc rax
// 00000006  488B7C2418        mov rdi,[rsp+0x18]
// 0000000B  488B742410        mov rsi,[rsp+0x10]
// 00000010  488B542408        mov rdx,[rsp+0x8]
// 00000015  0F05              syscall
// 00000017  C3                ret
#define CG_P_WRITE \
	"4831c048ffc0488b7c2418488b742410488b5424080f05c3"

// t.memcomp:
// 00000000  488B742418        mov rsi,[rsp+0x18]
// 00000005  488B7C2410        mov rdi,[rsp+0x10]
// 0000000A  488B4C2408        mov rcx,[rsp+0x8]
// 0000000F  48FFC1            inc rcx
// 00000012  FC                cld
// 00000013  F3A6              repe cmpsb
// 00000015  4809C9            or rcx,rcx
// 00000018  0F8504000000      jnz near 0x22
// 0000001E  4831C0            xor rax,rax
// 00000021  C3                ret
// 00000022  8A46FF            mov al,[rsi-0x1]
// 00000025  2A47FF            sub al,[rdi-0x1]
// 00000028  6698              cbw
// 0000002A  6699              cwd
// 0000002C  C3                ret
#define CG_P_MEMCOMP \
	"488b742418488b7c2410488b4c240848ffc1fcf3a64809c90f85040000004831c0c38a46ff2a47ff66986699c3"

// t.memcopy:
// 00000000  488B742418        mov rsi,[rsp+0x18]
// 00000005  488B7C2410        mov rdi,[rsp+0x10]
// 0000000A  488B4C2408        mov rcx,[rsp+0x8]
// 0000000F  FC                cld
// 00000010  F3A4              rep movsb
// 00000012  C3                ret
#define CG_P_MEMCOPY \
	"488b742418488b7c2410488b4c2408fcf3a4c3"

// t.memfill:
// 00000000  488B7C2418        mov rdi,[rsp+0x18]
// 00000005  488B442410        mov rax,[rsp+0x10]
// 0000000A  488B4C2408        mov rcx,[rsp+0x8]
// 0000000F  FC                cld
// 00000010  F3AA              rep stosb
// 00000012  C3                ret
#define CG_P_MEMFILL \
	"488b7c2418488b442410488b4c2408fcf3aac3"

// t.memscan:
// 00000000  488B7C2418        mov rdi,[rsp+0x18]
// 00000005  488B442410        mov rax,[rsp+0x10]
// 0000000A  488B4C2408        mov rcx,[rsp+0x8]
// 0000000F  48FFC1            inc rcx
// 00000012  4889FA            mov rdx,rdi
// 00000015  FC                cld
// 00000016  F2AE              repne scasb
// 00000018  4809C9            or rcx,rcx
// 0000001B  0F840A000000      jz near 0x2b
// 00000021  4889F8            mov rax,rdi
// 00000024  4829D0            sub rax,rdx
// 00000027  48FFC8            dec rax
// 0000002A  C3                ret
// 0000002B  4831C0            xor rax,rax
// 0000002E  48FFC8            dec rax
// 00000031  C3                ret
#define CG_P_MEMSCAN \
	"488b7c2418488b442410488b4c240848ffc14889fafcf2ae4809c90f840a0000004889f84829d048ffc8c34831c048ffc8c3"

// t.popcnt:
// 00000000  F3480FB8442408    popcnt rax,qword [rsp+0x8]
// 00000007  C3                ret
#define CG_P_POPCNT \
	"f3480fb8442408c3"

// 00000000  B802000000        mov eax,0x2
// 00000005  488B7C2418        mov rdi,[rsp+0x18]
// 0000000A  488B742410        mov rsi,[rsp+0x10]
// 0000000F  488B542408        mov rdx,[rsp+0x8]
// 00000014  0F05              syscall
// 00000016  C3                ret
#define CG_P_OPEN \
	"b802000000488b7c2418488b742410488b5424080f05c3"

// 00000000  B803000000        mov eax,0x3
// 00000005  488B7C2408        mov rdi,[rsp+0x8]
// 0000000A  0F05              syscall
// 0000000C  C3                ret
#define CG_P_CLOSE \
	"b803000000488b7c24080f05c3"

void gen(char *s, int64_t v);

void spill(void) {
	if (Loaded)
		gen(CG_PUSH, 0);
	else
		Loaded = 1;
}

uint8_t loaded(void) {
	return Loaded;
}

void clear(void) {
	Loaded = 0;
}

int8_t hex(char c) {
	if (isdigit(c))
		return c-'0';
	else
		return c-'a'+10;
}

void emit8(byte x) {
	Text[Tp++] = x;
}

void emit32(int32_t x) {
	emit8((byte)(255&x));
	emit8((byte)(255&x>>8));
	emit8((byte)(255&x>>16));
	emit8((byte)(255&x>>24));
}

void emit64(int64_t x) {
	emit8((byte)(255&x));
	emit8((byte)(255&x>>8));
	emit8((byte)(255&x>>16));
	emit8((byte)(255&x>>24));
	emit8((byte)(255&x>>32));
	emit8((byte)(255&x>>40));
	emit8((byte)(255&x>>48));
	emit8((byte)(255&x>>56));
}

void tpatch32(int64_t a, int32_t x) {
	Text[a] = (byte)(255&x);
	Text[a+1] = (byte)(255&x>>8);
	Text[a+2] = (byte)(255&x>>16);
	Text[a+3] = (byte)(255&x>>24);
}

void tpatch64(int64_t a, int64_t x) {
	Text[a] = (byte)(255&x);
	Text[a+1] = (byte)(255&x>>8);
	Text[a+2] = (byte)(255&x>>16);
	Text[a+3] = (byte)(255&x>>24);
	Text[a+4] = (byte)(255&x>>32);
	Text[a+5] = (byte)(255&x>>40);
	Text[a+6] = (byte)(255&x>>48);
	Text[a+7] = (byte)(255&x>>56);
}

int64_t tfetch(int64_t a) {
	return Text[a] | (Text[a+1]<<8) | (Text[a+2]<<16) | (Text[a+3]<<24);
}

void data8(byte x) {
	Data[Dp++] = x;
}

void data64(int64_t x) {
	data8((byte)(255&x));
	data8((byte)(255&x>>8));
	data8((byte)(255&x>>16));
	data8((byte)(255&x>>24));
	data8((byte)(255&x>>32));
	data8((byte)(255&x>>40));
	data8((byte)(255&x>>48));
	data8((byte)(255&x>>24));
}

void dpatch(int64_t a, int64_t x) {
	Data[a] = (byte)(255&x);
	Data[a+1] = (byte)(255&x>>8);
	Data[a+2] = (byte)(255&x>>16);
	Data[a+3] = (byte)(255&x>>24);
	Data[a+4] = (byte)(255&x>>32);
	Data[a+5] = (byte)(255&x>>40);
	Data[a+6] = (byte)(255&x>>48);
	Data[a+7] = (byte)(255&x>>56);
}

int64_t dfetch(int64_t a) {
	return Data[a]
		| ((int64_t)Data[a+1]<<8)
		| ((int64_t)Data[a+2]<<16)
		| ((int64_t)Data[a+3]<<24)
		| ((int64_t)Data[a+4]<<32)
		| ((int64_t)Data[a+5]<<40)
		| ((int64_t)Data[a+6]<<48)
		| ((int64_t)Data[a+7]<<56);
}

void tag(char seg) {
	if (Rp >= NRELOC)
		oops("relocation buffer overflow", NULL);
	Rel[Rp].seg = seg;
	Rel[Rp].addr = 't' == seg? Tp-BPW: Dp-BPW;
	Rp++;
}

void resolve(void) {
	int64_t i, dist, a;

	dist = DATA_VADDR + HEADER_SIZE + Tp;
	for (i=0; i<Rp; i++) {
		if ('t' == Rel[i].seg) {
			a = tfetch(Rel[i].addr);
			a += dist;
			tpatch64(Rel[i].addr, a);
		}
		else {
			a = dfetch(Rel[i].addr);
			a += dist;
			dpatch(Rel[i].addr, a);
		}
	}
}

void gen(char *s, int64_t v) {
	int64_t x;

	while (*s) {
		if (',' == *s) {
			if ('b' == s[1]) {
				emit8((byte)v);
			}
			else if ('w' == s[1]) {
				emit32((int32_t)v);
			}
			else if ('d' == s[1]) {
				emit64(v);
			}
			else if ('a' == s[1]) {
				emit64(v);
				tag('t');
			}
			else if ('m' == s[1]) {
				push(Tp);
			}
			else if ('>' == s[1]) {
				push(Tp);
				emit32(0);
			}
			else if ('<' == s[1]) {
				emit32((int32_t)(pop()-Tp-BYTES_REL));
			}
			else if ('r' == s[1]) {
				x = pop();
				tpatch32(x, (int32_t)(Tp-x-BYTES_REL));
			}
			else {
				oops("bad code", NULL);
			}
		}
		else {
			emit8((byte)(hex(*s)*16+hex(s[1])));
		}
		s += 2;
	}
}

void builtin(char *name, uint32_t arity, char *code) {
	gen(CG_JUMPFWD, 0);
	add(name, GLOBF|FUNC | (arity << 8), Tp);
	gen(code, 0);
	gen(CG_RESOLV, 0);
}

int64_t align(int64_t x, int64_t a) {
	return (x+a) & ~(a-1);
}

void writec(int c) {
	write(OutputFile, &c, 1);
}

void hexwrite(char *b) {
	while (*b) {
		writec(16*hex(*b)+hex(b[1]));
		b += 2;
	}
}

void lewrite32(int32_t x) {
	writec(x & 0xff);
	writec(x>>8 & 0xff);
	writec(x>>16 & 0xff);
	writec(x>>24 & 0xff);
}

void lewrite64(int64_t x) {
	writec((char)(x & 0xff));
	writec((char)(x>>8 & 0xff));
	writec((char)(x>>16 & 0xff));
	writec((char)(x>>24 & 0xff));
	writec((char)(x>>32 & 0xff));
	writec((char)(x>>40 & 0xff));
	writec((char)(x>>48 & 0xff));
	writec((char)(x>>56 & 0xff));
}

void elfheader(void) {
	hexwrite("7f454c46");	/* magic */
	hexwrite("02");			/* 64-bit */
	hexwrite("01");			/* little endian */
	hexwrite("01");			/* header version */
	hexwrite("03");			/* Linux ABI */
	hexwrite("0000000000000000");	/* padding */
	hexwrite("0200");		/* executable */
	hexwrite("3e00");		/* amd64 */
	lewrite32(1);			/* version */
	lewrite64(TEXT_VADDR+HEADER_SIZE);/* initial entry point */
	lewrite64(0x40);		/* program header offset */
	lewrite64(0);			/* no header segments */
	lewrite32(0);			/* flags */
	hexwrite("4000");		/* header size */
	hexwrite("3800");		/* program header size */
	hexwrite("0200");		/* number of program headers */
	hexwrite("4000");		/* segment header size (unused) */
	hexwrite("0000");		/* number of segment headers */
	hexwrite("0000");		/* string index (unused) */
	/* text segment */
	lewrite32(0x01);		/* loadable segment */
	lewrite32(0x05);		/* flags = read, execute */
	lewrite64(HEADER_SIZE);	/* offset in file */
	lewrite64(TEXT_VADDR+HEADER_SIZE);/* virtual load address */
	lewrite64(TEXT_VADDR+HEADER_SIZE);/* physical load address */
	lewrite64(Tp);			/* size in file */
	lewrite64(Tp);			/* size in memory */
	lewrite64(PAGE_SIZE);	/* alignment (page) */
	/* data segment */
	lewrite32(0x01);		/* loadable segment */
	lewrite32(0x06);		/* flags = read, write */
	lewrite64(HEADER_SIZE+Tp);	/* offset in file */
	lewrite64(DATA_VADDR+HEADER_SIZE+Tp);/* virtual load address */
	lewrite64(DATA_VADDR+HEADER_SIZE+Tp);/* physical load address */
	lewrite64(Dp);			/* size in file */
	lewrite64(Dp);			/* size in memory */
	lewrite64(PAGE_SIZE);	/* alignment (page) */
}

/*
 * Scanner
 */

char	Prog[PROG_SIZE];

ssize_t	Pp = 0, Psize;

void argparse(int argc, char *argv[]) {
	if (argc != 3) {
		fputs("invalid arguments\n", stderr);
		fputs("usage: ", stderr);
		fputs(argv[0], stderr);
		fputs(" <t-file> <output>\n", stderr);
		exit(1);
	}
	InputFile = open(argv[1], O_RDONLY, 0);
	if (InputFile < 0) {
		aw("couldn't open input file", 0);
	}
	OutputFile = open(argv[2], O_TRUNC | O_CREAT | O_WRONLY, 0755);
	if (OutputFile < 0) {
		close(InputFile);
		aw("couldn't open output file", 0);
	}
}

void readprog(void) {
	Psize = read(InputFile, Prog, PROG_SIZE);
	if (Psize >= PROG_SIZE)
		aw("program too big", NULL);
}

int32_t readrc(void) {
	return Pp >= Psize? EOF: Prog[Pp++];
}

int32_t readc(void) {
	return Pp >= Psize? EOF: tolower(Prog[Pp++]);
}

#define META		256

int32_t readec(void) {
	int32_t	c;

	c = readrc();
	if (c != '\\')
		return c;
	c = readc();
	if ('a' == c) return '\a';
	if ('b' == c) return '\b';
	if ('e' == c) return '\033';
	if ('f' == c) return '\f';
	if ('n' == c) return '\n';
	if ('q' == c) return '"' | META;
	if ('r' == c) return '\r';
	if ('s' == c) return ' ';
	if ('t' == c) return '\t';
	if ('v' == c) return '\v';
	return c;
}

void reject(void) {
	Pp--;
}

#define TOKEN_LEN	128

int32_t	T;
char	Str[TOKEN_LEN];
int64_t	Val;
int32_t	Oid;

int32_t	Equal_op, Minus_op, Mul_op, Add_op;

struct _oper {
	int32_t	prec;
	size_t	len;
	char	*name;
	int32_t	tok;
	char	*code;
};

#define oper	struct _oper

enum {	ENDFILE = -1,
	SYMBOL = 100, INTEGER, STRING,
	ADDROF = 200, ASSIGN, BINOP, BYTEOP, COLON, COMMA, COND,
	CONJ, DISJ, LBRACK, LPAREN, RBRACK, RPAREN, SEMI, UNOP,
	KCONST, KDECL, KDO, KELSE, KEND, KFOR, KHALT, KIE, KIF,
	KLEAVE, KLOOP, KRETURN, KSTRUCT, KVAR, KWHILE
};

oper Ops[] = {
	{ 7, 3, "mod",	BINOP,  CG_MOD		},
	{ 6, 1, "+",	BINOP,  CG_ADD		},
	{ 7, 1, "*",	BINOP,  CG_MUL		},
	{ 0, 1, ";",	SEMI,   NULL		},
	{ 0, 1, ",",	COMMA,  NULL		},
	{ 0, 1, "(",	LPAREN, NULL		},
	{ 0, 1, ")",	RPAREN, NULL		},
	{ 0, 1, "[",	LBRACK, NULL		},
	{ 0, 1, "]",	RBRACK, NULL		},
	{ 3, 1, "=",	BINOP,  CG_EQ		},
	{ 5, 1, "&",	BINOP,  CG_AND		},
	{ 5, 1, "|",	BINOP,  CG_OR		},
	{ 5, 1, "^",	BINOP,  CG_XOR		},
	{ 0, 1, "@",	ADDROF, NULL		},
	{ 0, 1, "~",	UNOP,   CG_INV		},
	{ 0, 1, ":",	COLON,  NULL		},
	{ 0, 2, "::",	BYTEOP, NULL		},
	{ 0, 2, ":=",	ASSIGN, NULL		},
	{ 0, 1, "\\",	UNOP,   CG_LOGNOT	},
	{ 1, 2, "\\/",	DISJ,   NULL		},
	{ 3, 2, "\\=",	BINOP,  CG_NEQ		},
	{ 4, 1, "<",	BINOP,  CG_LT		},
	{ 4, 2, "<=",	BINOP,  CG_LE		},
	{ 5, 2, "<<",	BINOP,  CG_SHL		},
	{ 4, 1, ">",	BINOP,  CG_GT		},
	{ 4, 2, ">=",   BINOP,  CG_GE		},
	{ 5, 2, ">>",	BINOP,  CG_SHR		},
	{ 6, 1, "-",	BINOP,  CG_SUB		},
	{ 0, 2, "->",	COND,   NULL		},
	{ 7, 1, "/",	BINOP,  CG_DIV		},
	{ 2, 2, "/\\",	CONJ,   NULL		},
	{ 0, 0, NULL,   0,      NULL		}
};

int32_t skip(void) {
	int32_t	c;

	c = readc();
	for (;;) {
		while (' ' == c || '\t' == c || '\n' == c || '\r' == c) {
			if ('\n' == c)
				Line++;
			c = readc();
		}
		if (c != '!')
			return c;
		while (c != '\n' && c != EOF)
			c = readc();
	}
}

int32_t findkw(char *s) {
	if ('c' == s[0]) {
		if (!strcmp(s, "const")) return KCONST;
		return 0;
	}
	if ('d' == s[0]) {
		if (!strcmp(s, "do")) return KDO;
		if (!strcmp(s, "decl")) return KDECL;
		return 0;
	}
	if ('e' == s[0]) {
		if (!strcmp(s, "else")) return KELSE;
		if (!strcmp(s, "end")) return KEND;
		return 0;
	}
	if ('f' == s[0]) {
		if (!strcmp(s, "for")) return KFOR;
		return 0;
	}
	if ('h' == s[0]) {
		if (!strcmp(s, "halt")) return KHALT;
		return 0;
	}
	if ('i' == s[0]) {
		if (!strcmp(s, "if")) return KIF;
		if (!strcmp(s, "ie")) return KIE;
		return 0;
	}
	if ('l' == s[0]) {
		if (!strcmp(s, "leave")) return KLEAVE;
		if (!strcmp(s, "loop")) return KLOOP;
		return 0;
	}
	if ('m' == s[0]) {
		if (!strcmp(s, "mod")) return BINOP;
		return 0;
	}
	if ('r' == s[0]) {
		if (!strcmp(s, "return")) return KRETURN;
		return 0;
	}
	if ('s' == s[0]) {
		if (!strcmp(s, "struct")) return KSTRUCT;
		return 0;
	}
	if ('v' == s[0]) {
		if (!strcmp(s, "var")) return KVAR;
		return 0;
	}
	if ('w' == s[0]) {
		if (!strcmp(s, "while")) return KWHILE;
		return 0;
	}
	return 0;
}

int32_t scanop(int32_t c) {
	int32_t	i;
	size_t j;

	i = 0;
	j = 0;
	Oid = -1;
	while (Ops[i].len > 0) {
		if (Ops[i].len > j) {
			if (Ops[i].name[j] == c) {
				Oid = i;
				Str[j] = (char)c;
				c = readc();
				j++;
			}
		}
		else {
			break;
		}
		i++;
	}
	if (-1 == Oid) {
		Str[j++] = (char)c;
		Str[j] = 0;
		aw("unknown operator", Str);
	}
	Str[j] = 0;
	reject();
	return Ops[Oid].tok;
}

void findop(char *s) {
	int32_t	i;

	i = 0;
	while (Ops[i].len > 0) {
		if (!strcmp(s, Ops[i].name)) {
			Oid = i;
			return;
		}
		i++;
	}
	oops("operator not found", s);
}

int32_t scan(void) {
	int32_t	c, i, k, sgn;

	c = skip();
	if (EOF == c) {
		strcpy(Str, "end of file");
		return ENDFILE;
	}
	if (isalpha(c) || '_' == c || '.' == c) {
		i = 0;
		while (isalpha(c) || '_' == c || '.' == c || isdigit(c)) {
			if (i >= TOKEN_LEN-1) {
				Str[i] = 0;
				aw("symbol too long", Str);
			}
			Str[i++] = (char)c;
			c = readc();
		}
		Str[i] = 0;
		reject();
		if ((k = findkw(Str)) != 0) {
			if (BINOP == k)
				findop(Str);
			return k;
		}
		return SYMBOL;
	}
	if (isdigit(c) || '%' == c) {
		sgn = 1;
		i = 0;
		if ('%' == c) {
			sgn = -1;
			c = readc();
			Str[i++] = (char)c;
			if (!isdigit(c)) {
				reject();
				return scanop('-');
			}
		}
		Val = 0;
		while (isdigit(c)) {
			if (i >= TOKEN_LEN-1) {
				Str[i] = 0;
				aw("integer too long", Str);
			}
			Str[i++] = (char)c;
			Val = Val * 10 + c - '0';
			c = readc();
		}
		Str[i] = 0;
		reject();
		Val = Val * sgn;
		return INTEGER;
	}
	if ('\'' == c) {
		Val = readec();
		if (readc() != '\'')
			aw("missing ''' in character", NULL);
		return INTEGER;
	}
	if ('"' == c) {
		i = 0;
		c = readec();
		while (c != '"' && c != EOF) {
			if (i >= TOKEN_LEN-1) {
				Str[i] = 0;
				aw("string too long", Str);
			}
			Str[i++] = (char)(c & (META-1));
			c = readec();
		}
		Str[i] = 0;
		return STRING;
	}
	return scanop(c);
}

/*
 * Parser
 */

#define MAXTBL		128
#define MAXLOOP		100

uint8_t	Fun = 0;
int64_t Loop0 = -1;
int64_t	Leaves[MAXLOOP], Lvp = 0;
int64_t	Loops[MAXLOOP], Llp = 0;

void expect(int32_t t, char *s) {
	char	b[100];

	if (t == T)
		return;
	sprintf(b, "%s expected", s);
	aw(b, Str);
}

void eqsign(void) {
	if (T != BINOP || Oid != Equal_op)
		expect(0, "'='");
	T = scan();
}

void semi(void) {
	expect(SEMI, "';'");
	T = scan();
}

void xlparen(void) {
	expect(LPAREN, "'('");
	T = scan();
}

void xrparen(void) {
	expect(RPAREN, "')'");
	T = scan();
}

int64_t constfac(void) {
	int64_t	v;
	sym	*y;

	if (INTEGER == T) {
		v = Val;
		T = scan();
		return v;
	}
	if (SYMBOL == T) {
		y = lookup(Str, CNST);
		T = scan();
		return y->value;
	}
	aw("constant value expected", Str);
	return 0; /*LINT*/
}

int64_t constval(void) {
	int64_t	v;

	v = constfac();
	if (BINOP == T && Mul_op == Oid) {
		T = scan();
		v *= constfac();
	}
	else if (BINOP == T && Add_op == Oid) {
		T = scan();
		v += constfac();
	}
	return v;
}

void vardecl(uint32_t glob) {
	sym	*y;
	int64_t	size;

	T = scan();
	while (1) {
		expect(SYMBOL, "symbol");
		size = 1;
		if (glob & GLOBF)
			y = add(Str, glob, Dp);
		else
			y = add(Str, 0, Lp);
		T = scan();
		if (LBRACK == T) {
			T = scan();
			size = constval();
			if (size < 1)
				aw("invalid size", NULL);
			y->flags |= VECT;
			expect(RBRACK, "']'");
			T = scan();
		}
		else if (BYTEOP == T) {
			T = scan();
			size = constval();
			if (size < 1)
				aw("invalid size", NULL);
			size = (size + BPW-1) / BPW;
			y->flags |= VECT;
		}
		if (glob & GLOBF) {
			if (y->flags & VECT) {
				gen(CG_ALLOC, size*BPW);
				GlobSize += size*BPW;
				gen(CG_GLOBVEC, Dp);
			}
			data64(0);
		}
		else {
			gen(CG_ALLOC, size*BPW);
			Lp -= size*BPW;
			if (y->flags & VECT) {
				gen(CG_LOCLVEC, 0);
				Lp -= BPW;
			}
			y->value = Lp;
		}
		if (T != COMMA)
			break;
		T = scan();
	}
	semi();
}

void constdecl(uint32_t glob) {
	sym	*y;

	T = scan();
	while (1) {
		expect(SYMBOL, "symbol");
		y = add(Str, glob|CNST, 0);
		T = scan();
		eqsign();
		y->value = constval();
		if (T != COMMA)
			break;
		T = scan();
	}
	semi();
}

void stcdecl(uint32_t glob) {
	sym	*y;
	int32_t	i;

	T = scan();
	expect(SYMBOL, "symbol");
	y = add(Str, glob|CNST, 0);
	T = scan();
	i = 0;
	eqsign();
	while (1) {
		expect(SYMBOL, "symbol");
		add(Str, glob|CNST, i++);
		T = scan();
		if (T != COMMA)
			break;
		T = scan();
	}
	y->value = i;
	semi();
}

void fwddecl(void) {
	sym	*y;
	int64_t	n;

	T = scan();
	while (1) {
		expect(SYMBOL, "symbol");
		y = add(Str, GLOBF|DECL, 0);
		T = scan();
		xlparen();
		n = constval();
		y->flags |= n << 8;
		xrparen();
		if (n < 0)
			aw("invalid arity", NULL);
		if (T != COMMA)
			break;
		T = scan();
	}
	semi();
}

void resolve_fwd(int64_t loc, int64_t fn) {
	int64_t	nloc;

	while (loc != 0) {
		nloc = tfetch(loc);
		tpatch32(loc, (int32_t)(fn-loc-BYTES_REL));
		loc = nloc;
	}
}

void compound(void);
void stmt(void);

void fundecl(void) {
	int64_t	l_base, l_addr = 0;
	int64_t	i, na = 0;
	int64_t	oyp;
	sym	*y;

	gen(CG_JUMPFWD, 0);
	y = add(Str, GLOBF|FUNC, Tp);
	T = scan();
	xlparen();
	oyp = Yp;
	l_base = Yp;
	while (SYMBOL == T) {
		add(Str, 0, l_addr);
		l_addr += BPW;
		na++;
		T = scan();
		if (T != COMMA)
			break;
		T = scan();
	}
	for (i = l_base; i < Yp; i++) {
		// na+1 because of the return address on the stack
		Sym[i].value = (na+1)*BPW - Sym[i].value;
	}
	if (y->flags & DECL) {
		resolve_fwd(y->value, Tp);
		if (na != y->flags >> 8)
			aw("redefinition with different type", y->name);
		y->flags &= ~DECL;
		y->flags |= FUNC;
		y->value = Tp;
	}
	xrparen();
	y->flags |= na << 8;
	gen(CG_ENTER, 0);
	Fun = 1;
	stmt();
	Fun = 0;
	gen(CG_CLEAR, 0);
	gen(CG_EXIT, 0);
	gen(CG_RESOLV, 0);
	Yp = oyp;
	Lp = 0;
}

void declaration(uint32_t glob) {
	if (KVAR == T)
		vardecl(glob);
	else if (KCONST == T)
		constdecl(glob);
	else if (KSTRUCT== T)
		stcdecl(glob);
	else if (KDECL == T)
		fwddecl();
	else
		fundecl();
}

void expr(uint8_t clr);

void fncall(sym *fn) {
	int32_t	i = 0;

	T = scan();
	if (NULL == fn)
		aw("call of non-function", NULL);
	while (T != RPAREN) {
		expr(0);
		i++;
		if (COMMA != T)
			break;
		T = scan();
		if (RPAREN == T)
			aw("syntax error", Str);
	}
	if (i != (fn->flags >> 8))
		aw("wrong number of arguments", fn->name);
	expect(RPAREN, "')'");
	T = scan();
	if (loaded())
		spill();
	if (fn->flags & DECL) {
		gen(CG_CALL, fn->value);
		fn->value = Tp-BYTES_REL;
	}
	else {
		gen(CG_CALL, fn->value-Tp-(BYTES_REL+1));
	}
	if (i != 0)
		gen(CG_DEALLOC, i*BPW);
	Loaded = 1;
}

int64_t mkstring(char *s) {
	int64_t	a;
	size_t i, k;

	a = Dp;
	k = strlen(s);
	for (i=0; i<=k; i++)
		data8((byte)s[i]);
	while (Dp % BPW != 0)
		data8(0);
	return a;
}

int64_t mktable(void) {
	int32_t	n, i;
	int64_t	loc;
	int64_t	tbl[MAXTBL], af[MAXTBL];
	uint8_t	dynamic = 0;

	T = scan();
	n = 0;
	while (T != RBRACK) {
		if (n >= MAXTBL)
			aw("table too big", NULL);
		if (LPAREN == T) {
			T = scan();
			dynamic = 1;
			continue;
		}
		else if (dynamic) {
			expr(1);
			gen(CG_STGLOB, 0);
			tbl[n] = 0;
			af[n++] = Tp-BPW;
			if (RPAREN == T) {
				T = scan();
				dynamic = 0;
			}
		}
		else if (INTEGER == T || SYMBOL == T) {
			tbl[n] = constval();
			af[n++] = 0;
		}
		else if (STRING == T) {
			tbl[n] = mkstring(Str);
			af[n++] = 1;
			T = scan();
		}
		else if (LBRACK == T) {
			tbl[n] = mktable();
			af[n++] = 1;
		}
		else {
			aw("invalid table element", Str);
		}
		if (T != COMMA)
			break;
		T = scan();
	}
	expect(RBRACK, "']'");
	T = scan();
	loc = Dp;
	for (i=0; i<n; i++) {
		data64(tbl[i]);
		if (1 == af[i]) {
			tag('d');
		}
		else if (af[i] > 1) {
			tpatch64(af[i], Dp-BPW);
		}
	}
	return loc;
}

void load(sym *y) {
	if (y->flags & GLOBF)
		gen(CG_LDGLOB, y->value);
	else
		gen(CG_LDLOCL, y->value);
}

void store(sym *y) {
	if (y->flags & GLOBF)
		gen(CG_STGLOB, y->value);
	else
		gen(CG_STLOCL, y->value);
}

void factor(void);

sym *address(uint8_t lv, uint8_t *bp) {
	sym	*y;

	y = lookup(Str, 0);
	T = scan();
	if (y->flags & CNST) {
		if (lv > 0) aw("invalid address", y->name);
		spill();
		gen(CG_LDVAL, y->value);
	}
	else if (y->flags & (FUNC|DECL)) {
		if (2 == lv) aw("invalid address", y->name);
	}
	else if (0 == lv || LBRACK == T || BYTEOP == T) {
		spill();
		load(y);
	}
	if (LBRACK == T || BYTEOP == T)
		if (y->flags & (FUNC|DECL|CNST))
			aw("bad subscript", y->name);
	while (LBRACK == T) {
		*bp = 0;
		T = scan();
		expr(0);
		expect(RBRACK, "']'");
		T = scan();
		y = NULL;
		gen(CG_INDEX, 0);
		if (LBRACK == T || BYTEOP == T || 0 == lv)
			gen(CG_DEREF, 0);
	}
	if (BYTEOP == T) {
		*bp = 1;
		T = scan();
		factor();
		y = NULL;
		gen(CG_INDXB, 0);
		if (0 == lv)
			gen(CG_DREFB, 0);
	}
	return y;
}

void factor(void) {
	sym	*y;
	int32_t	op;
	uint8_t	b;

	if (INTEGER == T) {
		spill();
		gen(CG_LDVAL, Val);
		T = scan();
	}
	else if (SYMBOL == T) {
		y = address(0, &b);
		if (LPAREN == T) {
			fncall(y);
		}
	}
	else if (STRING == T) {
		spill();
		gen(CG_LDADDR, mkstring(Str));
		T = scan();
	}
	else if (LBRACK == T) {
		spill();
		gen(CG_LDADDR, mktable());
	}
	else if (ADDROF == T) {
		T = scan();
		y = address(2, &b);
		if (NULL == y) {
			;
		}
		else if (y->flags & GLOBF) {
			spill();
			gen(CG_LDADDR, y->value);
		}
		else {
			spill();
			gen(CG_LDLREF, y->value);
		}
	}
	else if (BINOP == T) {
		op = Oid;
		if (Oid != Minus_op)
			aw("syntax error", Str);
		T = scan();
		factor();
		gen(CG_NEG, 0);
	}
	else if (UNOP == T) {
		op = Oid;
		T = scan();
		factor();
		gen(Ops[op].code, 0);
	}
	else if (LPAREN == T) {
		T = scan();
		expr(0);
		xrparen();
	}
	else {
		aw("syntax error", Str);
	}
}

int32_t emitop(int32_t *stk, int32_t sp) {
	gen(Ops[stk[sp-1]].code, 0);
	return sp-1;
}

void arith(void) {
	int32_t	stk[10], sp;

	sp = 0;
	factor();
	while (BINOP == T) {
		while (sp && Ops[Oid].prec <= Ops[stk[sp-1]].prec)
			sp = emitop(stk, sp);
		stk[sp++] = Oid;
		T = scan();
		factor();
	}
	while (sp > 0) {
		sp = emitop(stk, sp);
	}
}

void conjn(void) {
	int32_t	n = 0;

	arith();
	while (CONJ == T) {
		T = scan();
		gen(CG_JMPFALSE, 0);
		clear();
		arith();
		n++;
	}
	while (n > 0) {
		gen(CG_RESOLV, 0);
		n--;
	}
}

void disjn(void) {
	int32_t	n = 0;

	conjn();
	while (DISJ == T) {
		T = scan();
		gen(CG_JMPTRUE, 0);
		clear();
		conjn();
		n++;
	}
	while (n > 0) {
		gen(CG_RESOLV, 0);
		n--;
	}
}

void expr(uint8_t clr) {
	if (clr) {
		clear();
	}
	disjn();
	if (COND == T) {
		T = scan();
		gen(CG_JMPFALSE, 0);
		expr(1);
		expect(COLON, "':'");
		T = scan();
		gen(CG_JUMPFWD, 0);
		swap();
		gen(CG_RESOLV, 0);
		expr(1);
		gen(CG_RESOLV, 0);
	}
}

void stmt(void);

void halt_stmt(void) {
	T = scan();
	gen(CG_HALT, constval());
	semi();
}

void return_stmt(void) {
	T = scan();
	if (0 == Fun)
		aw("can't return from main body", 0);
	if (SEMI == T)
		gen(CG_CLEAR, 0);
	else
		expr(1);
	if (Lp != 0) {
		gen(CG_DEALLOC, -Lp);
	}
	gen(CG_EXIT, 0);
	semi();
}

void if_stmt(uint8_t alt) {
	T = scan();
	xlparen();
	expr(1);
	gen(CG_JMPFALSE, 0);
	xrparen();
	stmt();
	if (alt) {
		gen(CG_JUMPFWD, 0);
		swap();
		gen(CG_RESOLV, 0);
		expect(KELSE, "ELSE");
		T = scan();
		stmt();
	}
	else if (KELSE == T) {
		aw("ELSE without IE", NULL);
	}
	gen(CG_RESOLV, 0);
}

void while_stmt(void) {
	int64_t	olp, olv;

	olp = Loop0;
	olv = Lvp;
	T = scan();
	xlparen();
	gen(CG_MARK, 0);
	Loop0 = tos();
	expr(1);
	xrparen();
	gen(CG_JMPFALSE, 0);
	stmt();
	swap();
	gen(CG_JUMPBACK, 0);
	gen(CG_RESOLV, 0);
	while (Lvp > olv) {
		push(Leaves[Lvp-1]);
		gen(CG_RESOLV, 0);
		Lvp--;
	}
	Loop0 = olp;
}

void for_stmt(void) {
	sym	*y;
	int64_t	step = 1;
	int64_t	oll, olp, olv;

	T = scan();
	oll = Llp;
	olv = Lvp;
	olp = Loop0;
	Loop0 = 0;
	xlparen();
	expect(SYMBOL, "symbol");
	y = lookup(Str, 0);
	T = scan();
	if (y->flags & (CNST|FUNC|DECL))
		aw("unexpected type", y->name);
	eqsign();
	expr(1);
	store(y);
	expect(COMMA, "','");
	T = scan();
	gen(CG_MARK, 0);
	load(y);
	expr(0);
	if (COMMA == T) {
		T = scan();
		step = constval();
	}
	gen(step<0? CG_FORDOWN: CG_FOR, 0);
	xrparen();
	stmt();
	while (Llp > oll) {
		push(Loops[Llp-1]);
		gen(CG_RESOLV, 0);
		Llp--;
	}
	if (y->flags & GLOBF)
		gen(CG_INCGLOB, y->value);
	else
		gen(CG_INCLOCL, y->value);
	gen(CG_WORD, step);
	swap();
	gen(CG_JUMPBACK, 0);
	gen(CG_RESOLV, 0);
	while (Lvp > olv) {
		push(Leaves[Lvp-1]);
		gen(CG_RESOLV, 0);
		Lvp--;
	}
	Llp = oll;
	Loop0 = olp;
}

void leave_stmt(void) {
	if (Loop0 < 0)
		aw("LEAVE not in loop context", 0);
	T = scan();
	semi();
	if (Lvp >= MAXLOOP)
		aw("too many LEAVEs", NULL);
	gen(CG_JUMPFWD, 0);
	Leaves[Lvp++] = pop();
}

void loop_stmt(void) {
	if (Loop0 < 0)
		aw("LOOP not in loop context", 0);
	T = scan();
	semi();
	if (Loop0 > 0) {
		push(Loop0);
		gen(CG_JUMPBACK, 0);
	}
	else {
		if (Llp >= MAXLOOP)
			aw("too many LOOPs", NULL);
		gen(CG_JUMPFWD, 0);
		Loops[Llp++] = pop();
	}
}

void asg_or_call(void) {
	sym	*y;
	uint8_t	b;

	clear();
	y = address(1, &b);
	if (LPAREN == T) {
		fncall(y);
	}
	else if (ASSIGN == T) {
		T = scan();
		expr(0);
		if (NULL == y)
			gen(b? CG_STINDB: CG_STINDR, 0);
		else if (y->flags & (FUNC|DECL|CNST|VECT))
			aw("bad location", y->name);
		else
			store(y);
	}
	else {
		aw("syntax error", Str);
	}
	semi();
}

void stmt(void) {
	if (KFOR == T)
		for_stmt();
	else if (KHALT == T)
		halt_stmt();
	else if (KIE == T)
		if_stmt(1);
	else if (KIF == T)
		if_stmt(0);
	else if (KLEAVE == T)
		leave_stmt();
	else if (KLOOP == T)
		loop_stmt();
	else if (KRETURN == T)
		return_stmt();
	else if (KWHILE == T)
		while_stmt();
	else if (KDO == T)
		compound();
	else if (SYMBOL == T)
		asg_or_call();
	else if (SEMI == T)
		T = scan();
	else
		expect(0, "statement");
}

void compound(void) {
	int64_t	oyp, olp;

	expect(KDO, "DO");
	T = scan();
	oyp = Yp;
	olp = Lp;
	while (KVAR == T || KCONST == T || KSTRUCT == T)
		declaration(0);
	while (T != KEND)
		stmt();
	T = scan();
	if (olp - Lp != 0)
		gen(CG_DEALLOC, olp-Lp);
	Yp = oyp;
	Lp = olp;
}

void program(void) {
	int32_t	i;
	gen(CG_ARGV, 0);

	T = scan();
	while (	KVAR == T || KCONST == T || SYMBOL == T ||
		KDECL == T || KSTRUCT == T
	)
		declaration(GLOBF);
	if (T != KDO)
		aw("DO or declaration expected", NULL);

	gen(CG_INIT, 0);
	add("argv", VECT, GlobSize);
	add("argc", 0, GlobSize+BPW);
	compound();
	gen(CG_HALT, 0);
	for (i=0; i<Yp; i++)
		if (Sym[i].flags & DECL && Sym[i].value)
			aw("undefined function", Sym[i].name);
}

/*
 * Main
 */

void init(void) {
	findop("="); Equal_op = Oid;
	findop("-"); Minus_op = Oid;
	findop("*"); Mul_op = Oid;
	findop("+"); Add_op = Oid;
	builtin("t.read", 3, CG_P_READ);
	builtin("t.write", 3, CG_P_WRITE);
	builtin("t.memcomp", 3, CG_P_MEMCOMP);
	builtin("t.memcopy", 3, CG_P_MEMCOPY);
	builtin("t.memfill", 3, CG_P_MEMFILL);
	builtin("t.memscan", 3, CG_P_MEMSCAN);
	builtin("t.popcnt", 1, CG_P_POPCNT);
	builtin("t.open", 3, CG_P_OPEN);
	builtin("t.close", 1, CG_P_CLOSE);
}

int main(int argc, char *argv[]) {
	init();
	argparse(argc, argv);
	readprog();
	program();
	Tp = align(HEADER_SIZE+Tp, 16)-HEADER_SIZE; /* 16-byte align in file */
	resolve();
	elfheader();
	write(OutputFile, Text, (size_t)Tp);
	write(OutputFile, Data, (size_t)Dp);
	return 0;
}

// Local Variables:
// indent-tabs-mode: t
// tab-width: 4
// End:
