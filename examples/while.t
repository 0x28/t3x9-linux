do
    var tbl, i;
    tbl := [["line1\n", 0],
            ["line2\n", 1],
            ["line3\n", 1],
            [%1,       %1]];
    i := 0;

    while (tbl[i][0] \= %1) do
        if (tbl[i][1] = 1)
            t.write(1, tbl[i][0], 6);
    	i := i+1;
    end
end
