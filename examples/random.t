do
    const BUFFER_SIZE = 30;
    var buffer::BUFFER_SIZE;
    var i, res;

    t.memfill(buffer, 0, BUFFER_SIZE);
    res := t.getrandom(buffer, BUFFER_SIZE, 0);

    if(res \= BUFFER_SIZE)
    do
        t.write(1, "not enough entropy\n", 19);
        halt 1;
    end

    for(i=0,BUFFER_SIZE)
    do
        buffer::i := buffer::i mod 26 + 'A';
    end
    t.write(1, buffer, BUFFER_SIZE);
    t.write(1, "\n", 1);
end
