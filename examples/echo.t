do
    const SIZE = 100;
    var input::SIZE;
    var read_size;
    read_size := t.read(0, input, SIZE);
    t.write(1, input, read_size);
end
