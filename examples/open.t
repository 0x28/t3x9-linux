do
    var f;
    f := t.open("test", 0, 0);
    if (f < 0) do
        t.write(2, "couldn't open file 'test'\n", 27);
        halt 1;
    end

    t.close(f);
end
