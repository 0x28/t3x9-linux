str.length(s) return t.memscan(s, 0, 32767);

! allocated on the stack -> shouldn't influence argv and argc
var vec::100;

do
    var buffer, i;

    buffer := '0' + argc;
    t.write(1, @buffer, 1);
    t.write(1, "\n", 1);
    for(i=0,argc) do
        t.write(1, argv[i], str.length(argv[i]));
        t.write(1, "\n", 1);
    end
end
