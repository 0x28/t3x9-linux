do
    ie (t.memcomp("hello", "hello", 5) = 0)
        t.write(1, "equal\n", 6);
    else
        t.write(1, "not equal\n", 10);
end
