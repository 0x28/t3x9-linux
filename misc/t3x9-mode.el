;;; t3x9-mode.el --- Major mode for the T3X9 programming language -*- lexical-binding: t; coding: utf-8 -*-
;; Version: 0.1.0

;;; Commentary:

;; Simple syntax highlighting for the T3X9 programming language. Use M-x
;; `package-install-from-buffer' to install this file.

;;; Code:

(defvar t3x9-font-lock-keywords
  '("const" "var" "do" "end" "while" "if" "for" "return" "struct" "ie" "else"
    "decl" "halt" "leave" "loop" "mod"))

(defconst t3x9--function-regexp "^\\([A-Za-z_][A-Za-z_0-9.]+\\)(")

;;;###autoload
(define-derived-mode t3x9-mode prog-mode "T3X9"
  "Major mode for the T3X9 programming language."
  (setq-local comment-start "!")
  (setq-local comment-end "")
  (setq-local comment-use-syntax t)
  (setq-local imenu-create-index-function #'t3x9--imenu-create-index)

  (font-lock-add-keywords
   nil
   `((,t3x9--function-regexp 1 font-lock-function-name-face)
     ("\\_<[0-9]+\\_>" . font-lock-constant-face)
     ("\\_<[A-Z_][A-Z_0-9]+\\_>" . font-lock-constant-face)
     (,(regexp-opt t3x9-font-lock-keywords 'words) . font-lock-keyword-face)))

  (modify-syntax-entry ?+ ".")
  (modify-syntax-entry ?- ".")
  (modify-syntax-entry ?* ".")
  (modify-syntax-entry ?/ ".")
  (modify-syntax-entry ?' "\"")
  (modify-syntax-entry ?! "<" t3x9-mode-syntax-table)
  (modify-syntax-entry ?\n ">" t3x9-mode-syntax-table))

(defun t3x9--imenu-create-index ()
  "Create a imenu index for T3X9 files."
  (let (index)
    (save-excursion
      (goto-char (point-max))
      (while (search-backward-regexp t3x9--function-regexp nil t)
        (push (cons (match-string-no-properties 1)
                    (point))
              index)))
    index))

;;;###autoload
(add-to-list 'auto-mode-alist
             '("\\.t\\'" . t3x9-mode))

(provide 't3x9-mode)
;;; t3x9-mode.el ends here
